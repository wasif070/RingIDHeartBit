package server;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import org.apache.log4j.Logger;
import server.util.StorageQueue;

public class HeartBitServer extends Thread {

    private static HeartBitServer instance;
    private boolean running = false;
    private DatagramSocket serverSocket = null;
    private static final Logger logger = Logger.getLogger(HeartBitServer.class.getName());

    public static HeartBitServer getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new HeartBitServer();
        }
    }

    public void init(String IP, int port) {
        try {
            serverSocket = new DatagramSocket(port);
            logger.debug("Socket opened on port:" + port);
        } catch (SocketException sockEx) {
            logger.debug("SocketException-->", sockEx);
        }
    }

    public HeartBitServer() {
        running = true;
    }

    @Override
    public void run() {
        logger.debug("HeartBeatServer has been started");
        while (running) {
            try {
                byte data[] = new byte[64];
                DatagramPacket packet = new DatagramPacket(data, 64);
                serverSocket.receive(packet);
                StorageQueue.getInstance().getPacketQueue().add(packet);
            } catch (Exception e) {
                logger.fatal("HeartBeatServer Exception: ", e);
            }
        }
    }

    public void stopService() {
        running = false;
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (Exception e) {
            logger.fatal("HeartBeatServer Stop Exception: ", e);
        }
    }
}
