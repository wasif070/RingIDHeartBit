/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.dto;

/**
 *
 * @author USER
 */
public class NDBServerDTO {

    private long id;
    private String name;
    private String ip;
    private long clstrGrp;

    public NDBServerDTO(long id, String name, String ip) {
        this.id = id;
        this.name = name;
        this.ip = ip;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public long getCluserGroup() {
        return clstrGrp;
    }

    public void setCluserGroup(long clstrGrp) {
        this.clstrGrp = clstrGrp;
    }

    
}
