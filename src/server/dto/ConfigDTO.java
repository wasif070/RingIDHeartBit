/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.dto;

import java.util.List;

/**
 *
 * @author USER
 */
public class ConfigDTO {

    private String blacklist_id_list;
    private String phoneNumbers;
    private List<NDBServerDTO> servers;

    public String getBlacklist_id_list() {
        return blacklist_id_list;
    }

    public void setBlacklist_id_list(String blacklist_id_list) {
        this.blacklist_id_list = blacklist_id_list;
    }

    public String getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(String phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<NDBServerDTO> getServers() {
        return servers;
    }

    public void setServers(List<NDBServerDTO> servers) {
        this.servers = servers;
    }

}
