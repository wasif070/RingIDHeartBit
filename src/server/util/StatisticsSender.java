/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.util;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.apache.log4j.Logger;

/**
 *
 * @author reefat
 */
public class StatisticsSender extends Thread {

    private boolean running;
    private static StatisticsSender instance;
    private static final Logger logger = Logger.getLogger(StatisticsSender.class.getName());

    public StatisticsSender() {
        running = true;
    }

    public static StatisticsSender getInstance() {
        if (instance == null) {
            instance = new StatisticsSender();
        }
        return instance;
    }

    @Override
    public void run() {
        logger.info("StatisticsSender has been started successfully");
        while (running) {
            try {
                if (getMinute() == 1) {
                    sendStatistics();
                }
                Thread.sleep(60000);
            } catch (InterruptedException iex) {
                logger.error("Thread exception in StatisticsSender-->", iex);
            }
        }
    }

    public void stopService() {
        running = false;
        this.interrupt();
    }

    public void sendStatistics() {
        DBConnection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = con.connection.createStatement();

            int totalUsers = 0;
            try {
                rs = stmt.executeQuery("select count(id) as totalUsers from users");
                if (rs.next()) {
                    totalUsers = rs.getInt("totalUsers");
                }
            } catch (SQLException ex) {
            }

            int totalSessions = 0;
            int liveSessions = 0;
            try {
                rs = stmt.executeQuery("select sum(currentRegitration) as totalSessions,sum(liveSessions) as onlineUsers from authservers where playingRole=1 and serverStatus=1");
                if (rs.next()) {
                    totalSessions = rs.getInt("totalSessions");
                    liveSessions = rs.getInt("onlineUsers");
                }
            } catch (SQLException ex) {
            }

            int android = 0;
            int ios = 0;
            int windows = 0;
            int pc = 0;
            int web = 0;
            try {
                rs = stmt.executeQuery("select device,totalDevice from devicetypecounter");
                while (rs.next()) {
                    switch (rs.getInt("device")) {
                        case Constants.ANDROID:
                            android = rs.getInt("totalDevice");
                            break;
                        case Constants.I_PHONE:
                            ios = rs.getInt("totalDevice");
                            break;
                        case Constants.WINDOWS:
                            windows = rs.getInt("totalDevice");
                            break;
                        case Constants.PC:
                            pc = rs.getInt("totalDevice");
                            break;
                        case Constants.WEB:
                            web = rs.getInt("totalDevice");
                            break;
                    }
                }
            } catch (SQLException ex) {
            }

            String message = "users=" + totalUsers + ",ses=" + totalSessions + ",liv=" + liveSessions;
            message += ",andrd=" + android + ",ios=" + ios;
            message += ",win=" + windows + ",web=" + web + ",pc=" + pc;

            for (String phoneNumber : Constants.PHONE_NUMBERS_SPECIAL) {
                SMSSender smsSender = new SMSSender(phoneNumber, message);
                smsSender.start();
            }
        } catch (Exception ex) {
            logger.error("Exception in getStatistics-->", ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                }
            }
            if (con != null) {
                try {
                    databaseconnector.DBConnector.getInstance().freeConnection(con);
                } catch (Exception ex) {
                }
            }
        }
    }

    private int getMinute() {
        Calendar calendar = new GregorianCalendar();
        return calendar.get(Calendar.MINUTE);
    }

    public static void main(String[] args) {
        StatisticsSender.getInstance().start();
    }
}
