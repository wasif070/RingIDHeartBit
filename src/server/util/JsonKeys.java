/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.util;

/**
 *
 * @author USER
 */
public class JsonKeys {

    public static final String SERVERS = "servers";
    public static final String BLACKLIST_ID_LIST = "blacklist_id_list";
    public static final String PHONE_NUMBERS = "phoneNumbers";
    public static final String IP = "ip";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String SEND_SMS = "send_sms";
    public static final String CLUSTER_GROUP = "clstrGrp";
    public static final String GALERA_USER = "galeraUser";
    public static final String GALERA_PASSWORD = "galeraPassword";
    public static final String GALERA_SERVERS = "galeraServers";
    public static final String GALERA_PHONES = "galeraPhnNo";
}
