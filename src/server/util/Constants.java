package server.util;

import server.profile.ProductionProfile;

public class Constants extends ProductionProfile {

    public static final long EXPIRE_TIME = 60000L;
    public static final long MESSAGE_SENT_INTERVAL = 300000L;
    //----------  CONFIG PARAMETERS ----------
    public static final String BIND_IP = "BIND_IP";
    public static final String BIND_PORT = "BIND_PORT";
    // ---------  SERVER TYPES ------------
    public static final int SERVER_TYPE_AUTH = 1;
    public static final int SERVER_TYPE_CHAT = 2;
//    public static final int SERVER_TYPE_OFFLINE = 3;
//    public static final int SERVER_TYPE_RELAY = 4;
    public static final int SERVER_TYPE_VOICE = 5;
    public static final int SERVER_TYPE_AUTH_CONTROLLER = 6;
//    public static final int SERVER_TYPE_IMAGE = 7;
    public static final int SERVER_TYPE_LOADBALANCER = 8;
    public static final int SERVER_TYPE_LIVE_STREAM = 9;
    public static final int SERVER_TYPE_CHANNEL = 10;
    public static final int SERVER_TYPE_COIN_SERVER = 11;
    public static final int OFFLINE_MEADIA_STREAMING_SERVER = 12;
    public static final int SERVER_TYPE_RINGBIT_SERVER = 13;
    public static final int SERVER_TYPE_SMS_BD_SERVER = 14;
    /* Server Type String */
    public static final String[] TYPE_TO_NAME = {"", "Auth Server", "Chat Server", "Offline Server", "Relay Server", "Voice Server", "Auth controller", "Image Server", "Load balancer", "Streaming Server", "Channel Server", "Coin Server", "Offline Media Streaming Server", "Ringbit Server", "smsbd Server"};
    // ---------  SERVER STATUS ------------
    public static final int SERVER_STATUS_DEAD = 0;
    public static final int SERVER_STATUS_ALIVE = 1;
    public static final String[] LIVE_STATUS_STRING = {"DEAD", "LIVE"};
    // ---------  SMS SENDING ------------
    public static boolean SEND_STATISTICS = true;
    public static String SMS_URL = "http://auth.ringid.com/sms/SMSRoutesHandler";
    public static String BRAND_ID = "8";
    public static String[] PHONE_NUMBERS = {"1717634317"};
    public static String[] PHONE_NUMBERS_SPECIAL = {"1717634317"};
    /*
     * DEVICE TYPES
     */
    public static final int PC = 1;
    public static final int ANDROID = 2;
    public static final int I_PHONE = 3;
    public static final int WINDOWS = 4;
    public static final int WEB = 5;
    public static final String COMMON_SUFFIX = "/index.html";
    public static final String LOADBALANCER_TYPE_AUTH_CONTROLLER = "ac";
    public static final String LOADBALANCER_TYPE_IMAGE = "img";
    public static final String LOADBALANCER_TYPE_CLOUD = "cloud";
    public static final String LOADBALANCER_TYPE_MOB_WEB = "mweb";
    public static final String LOADBALANCER_TYPE_WEB = "web";
    public static final String LOADBALANCER_HOST_MEDIA_DOT_RINGID_DOT_COM = "media.ringid.com";
    public static final String LOADBALANCER_HOST_RINGID_DOT_COM = "www.ringid.com";
    public static final String LOADBALANCER_HOST_AUTH_DOT_RINGID_DOT_COM = "auth.ringid.com";
    public static final String LOADBALANCER_HOST_IMAGES_DOT_RINGID_DOT_COM = "images.ringid.com";
    public static final int SECONDS_30 = 30000;
    public static final int MINUTES_3 = 3 * 60 * 1000;
    public static final int MINUTES_30 = 30 * 60 * 1000;
    public static final int MAX_ATTEMPT = 3;

    public static class AuthcontrollerReloadCase {

        public static final int SMSBD = 12;
    }

    public static boolean HEAVY_LOG_ENABLE = false;

    public static class CLUSTER_GROUP {

        public static final int ONE = 1;
        public static final int TWO = 2;
    }

}
