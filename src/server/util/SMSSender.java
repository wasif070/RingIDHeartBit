package server.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import org.apache.log4j.Logger;

public class SMSSender extends Thread {

    private static int READ_TIME_OUT = 10000;
    private static final String USER_AGENT = "Mozilla/5.0";
    private static Logger logger = Logger.getLogger(SMSSender.class.getName());
    private String destinationNumber;
    private String txtMessage;

    public SMSSender() {
    }

    public SMSSender(String mobileNumber, String message) {
        this.destinationNumber = mobileNumber;
        this.txtMessage = message;
    }

    @Override
    public void run() {
        StringBuilder response = new StringBuilder(512);
        try {
            URL obj = new URL(Constants.SMS_URL);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            String urlParameters = "countryCode=" + URLEncoder.encode("+880", "UTF-8");
            urlParameters += "&mobileNumber=" + URLEncoder.encode(destinationNumber, "UTF-8");
            urlParameters += "&textMessage=" + URLEncoder.encode(txtMessage, "UTF-8");
            if (!Constants.BRAND_ID.isEmpty()) {
                urlParameters += "&brandId=" + Constants.BRAND_ID;
            }
            // Send post request
            con.setReadTimeout(READ_TIME_OUT);
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

        } catch (Exception ex) {
            logger.error("Error during sendSMS", ex);
        }
    }
}
