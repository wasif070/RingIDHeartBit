/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.util;

import java.net.DatagramPacket;
import java.util.concurrent.ConcurrentLinkedQueue;
import server.db.ServerDTO;

/**
 *
 * @author Ashraful
 */
public class StorageQueue {

    private final ConcurrentLinkedQueue<DatagramPacket> packetQueue;
    //private final ConcurrentLinkedQueue<String> IPPorts;
    private final ConcurrentLinkedQueue<ServerDTO> IPPorts;
    private final ConcurrentLinkedQueue<ServerDTO> reloadQueue;
    private static StorageQueue instance;

    public StorageQueue() {
        packetQueue = new ConcurrentLinkedQueue<>();
        IPPorts = new ConcurrentLinkedQueue<>();
        reloadQueue = new ConcurrentLinkedQueue<>();
    }

    public static StorageQueue getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    public synchronized static void createInstance() {
        if (instance == null) {
            instance = new StorageQueue();
        }
    }

    public ConcurrentLinkedQueue<DatagramPacket> getPacketQueue() {
        return packetQueue;
    }

//    public ConcurrentLinkedQueue<String> getIPPorts() {
//        return IPPorts;
//    }
//
//    public void putIPPort(String ipPort) {
//        IPPorts.add(ipPort);
//    }
    public ConcurrentLinkedQueue<ServerDTO> getIPPorts() {
        return IPPorts;
    }

    public void putIPPort(ServerDTO ipPort) {
        IPPorts.add(ipPort);
    }

    public ConcurrentLinkedQueue<ServerDTO> getReloadQueue() {
        return reloadQueue;
    }

    public void putForReload(ServerDTO serverDTO) {
        if (serverDTO != null && Constants.SEND_STATISTICS) {
            reloadQueue.add(serverDTO);
        }
    }
}
