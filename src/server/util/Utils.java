/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.util;

import org.apache.log4j.Logger;

/**
 *
 * @author Ashraful
 */
public class Utils {

    private static final Logger logger = Logger.getLogger(Utils.class.getName());

    public static long ipToLong(String ip_address) {
        long result = 0;
        try {
            String[] ip_parts = ip_address.split("\\.");
            for (int i = 3; i > 0; i--) {
                long ip = Long.parseLong(ip_parts[3 - i]);
                result |= ip << (i * 8);
            }
            long ip = Long.parseLong(ip_parts[3]);
            result |= ip;
        } catch (IndexOutOfBoundsException | NumberFormatException indexEx) {
            logger.error("Exception in ipToLong-->", indexEx);
        }

        return result;
    }
}
