/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.log4j.Logger;

/**
 *
 * @author Faiz
 */
public class SendSmsService extends Thread {

    private final Logger LOGGER = Logger.getLogger(SendSmsService.class.getName());
    private boolean running = false;
    private final String[] mobileNumbers;
    private final String messageBody;
    private String smsURl = null;

    public SendSmsService(String[] mobileNumbers, String messageBody) {
        this.mobileNumbers = mobileNumbers;
        this.messageBody = messageBody;
        this.smsURl = Constants.SMS_URL;
    }

    public void sendSmsToAllMobileNumbers() {
        for (String mobileNumber : this.mobileNumbers) {
            String url = makeSMSURL(mobileNumber);
            String respons = httpGETResponse(url);
            if (respons.trim().equals("1")) {
                LOGGER.info("SMS status: Success, mobile: " + mobileNumber + " message: " + messageBody);
            } else {
                LOGGER.error("SMS status: Failed, mobile: " + mobileNumber + " message: " + messageBody);
            }
        }
    }

    /**
     * @param mobile
     * @return
     */
    private String makeSMSURL(String mobile) {
        StringBuilder bulder = new StringBuilder(this.smsURl + "?countryCode=%2B880&mobileNumber=");
        bulder.append(mobile);
        bulder.append("&textMessage=");
        bulder.append(messageBody);
        return bulder.toString();
    }

    private String httpGETResponse(String url) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            HttpURLConnection conn = null;
            BufferedReader rd = null;
            try {
                conn = (HttpURLConnection) new URL(url).openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Cache-Control", "no-cache");
                conn.setRequestProperty("Pragma", "no-cache");
                conn.setRequestProperty("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
                conn.setConnectTimeout(5000); //set timeout to 5 seconds
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);

                conn.setRequestProperty("User-Agent", "Mozilla/5.0");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;
                while ((line = rd.readLine()) != null) {
                    stringBuilder.append(line);
                }

            } catch (IOException e) {
                LOGGER.error(e, e);
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
                if (rd != null) {
                    rd.close();
                }
            }
        } catch (IOException e) {
            LOGGER.error(e, e);
        }
        return stringBuilder.toString();

    }

    @Override
    public void run() {
        try {
            if (!running) {
                running = true;
                sendSmsToAllMobileNumbers();
            }
        } catch (Exception e) {
        } finally {
            running = false;
        }
    }
}
