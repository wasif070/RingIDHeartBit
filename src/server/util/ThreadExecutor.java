/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author Ashraful
 */
public class ThreadExecutor {

    private final ExecutorService executorService;
    private static ThreadExecutor instance;

    public ThreadExecutor() {
        executorService = Executors.newFixedThreadPool(100);
    }

    public static ThreadExecutor getInstance() {
        if (instance == null) {
            instance = new ThreadExecutor();
        }
        return instance;
    }

    public void execute(Runnable runnable) {
        executorService.execute(runnable);
    }

    public Future submit(Runnable runnable) {
        return executorService.submit(runnable);
    }

    public void forceShutdown() {
        executorService.shutdownNow();
    }
}
