/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.util;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import server.dto.NDBServerDTO;
import server.galera.GaleraConstants;

/**
 *
 * @author USER
 */
public class LoadConfig {

    private static LoadConfig instance = null;
    private ArrayList<NDBServerDTO> serverList = new ArrayList<>();
    private boolean smsEnable = false;
    private final Logger LOGGER = Logger.getLogger(LoadConfig.class.getName());

    private LoadConfig() {
        if (instance != null) {
            throw new RuntimeException("Can not create instance use getInstance()");
        }
    }

    public static LoadConfig getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private static synchronized void createInstance() {
        if (instance == null) {
            instance = new LoadConfig();
        }
    }

    public boolean loadData() {
        JSONParser parser = new JSONParser();
        try {
            serverList = new ArrayList<>();
            Object obj = parser.parse(new FileReader("Config.json"));
            JSONObject jsonObject = (JSONObject) obj;
            if (jsonObject.containsKey(JsonKeys.SERVERS)) {
                JSONArray servers = (JSONArray) jsonObject.get(JsonKeys.SERVERS);
                for (int i = 0; i < servers.size(); i++) {
                    JSONObject jobject = (JSONObject) servers.get(i);
                    long id = (long) jobject.get(JsonKeys.ID);
                    String name = (String) jobject.get(JsonKeys.NAME);
                    String ip = (String) jobject.get(JsonKeys.IP);
                    long clusterGroup = (long) jobject.get(JsonKeys.CLUSTER_GROUP);
                    NDBServerDTO dto = new NDBServerDTO(id, name, ip);
                    dto.setCluserGroup(clusterGroup);
                    serverList.add(dto);
                }
                LOGGER.info("Total Culster=>" + serverList.size());
            } else {
                throw new NullPointerException("Server will exit, because of NBD cluster config");
            }
            if (jsonObject.containsKey(JsonKeys.GALERA_SERVERS)) {
                String galeraServers = (String) jsonObject.get(JsonKeys.GALERA_SERVERS);
                LOGGER.info("galeraServers=>" + galeraServers);
                String[] servers = galeraServers.split(",");
                GaleraConstants.galeraServers = new String[servers.length];
                System.arraycopy(servers, 0, GaleraConstants.galeraServers, 0, servers.length);
                if (jsonObject.containsKey(JsonKeys.GALERA_USER)) {
                    GaleraConstants.userName = (String) jsonObject.get(JsonKeys.GALERA_USER);
                    LOGGER.info("GaleraConstants.userName=>" + GaleraConstants.userName);
                } else {
                    throw new NullPointerException("Server will exit, Galera username not found");
                }
                if (jsonObject.containsKey(JsonKeys.GALERA_PASSWORD)) {
                    GaleraConstants.password = (String) jsonObject.get(JsonKeys.GALERA_PASSWORD);
                } else {
                    throw new NullPointerException("Server will exit, Galera password not found");
                }
                if (jsonObject.containsKey(JsonKeys.GALERA_PHONES)) {
                    String phones = (String) jsonObject.get(JsonKeys.GALERA_PHONES);
                    LOGGER.info("phoneNumbers=>" + phones);
                    String[] phoneNumbers = phones.split(",");
                    GaleraConstants.phoneNumbers = new String[phoneNumbers.length];
                    System.arraycopy(phoneNumbers, 0, GaleraConstants.phoneNumbers, 0, phoneNumbers.length);
                } else {
                    throw new NullPointerException("Server will exit, because Galera phone numbers not exits");
                }
                LOGGER.info("servers=>" + GaleraConstants.galeraServers.length + " GaleraConstants.phoneNumbers" + GaleraConstants.phoneNumbers.length);
            } else {
                throw new NullPointerException("Server will exit, because of Galera Info");
            }
            smsEnable = (boolean) jsonObject.get(JsonKeys.SEND_SMS);
            LOGGER.info("smsEnable=>" + smsEnable);
        } catch (IOException | ParseException | NullPointerException ex) {
            LOGGER.error(ex, ex);
            System.exit(0);
        }
        return true;
    }

    public ArrayList<NDBServerDTO> getServersList() {
        return serverList;
    }

    public boolean isSmsEnable() {
        return smsEnable;
    }

//    public static void main(String[] args) {
//        try {
//            LoadConfig.getInstance().loadData();
//        } catch (ParseException ex) {
//            ex.printStackTrace();
//        }
//    }
}
