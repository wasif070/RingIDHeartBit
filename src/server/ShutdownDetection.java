package server;

import java.io.File;
import java.util.Date;
import org.apache.log4j.Logger;
import server.db.StatusUpdateProcessor;
import server.galera.GaleraExecutorService;
import server.http.HTTPMonitor;
import server.ndbmonitor.ServerStatusChecker;
import server.util.Constants;
import server.util.StatisticsSender;

public class ShutdownDetection extends Thread {

    private boolean running = false;
    private static final long shutdownDetectionInterval = 1000;
    private static final File file = new File("shutdown.dat");
    static Logger logger = Logger.getLogger(ShutdownDetection.class.getName());

    public ShutdownDetection() {
        running = true;
        if (file.exists()) {
            try {
                file.delete();
            } catch (Exception e) {
                try {
                    Thread.sleep(shutdownDetectionInterval);
                    file.delete();
                } catch (Exception ex) {
                }
            }
        }
    }

    @Override
    public void run() {
        while (running) {
            if (file.exists()) {
                try {
                    file.delete();
                } catch (Exception e) {
                }
                break;
            }
            try {
                Thread.sleep(shutdownDetectionInterval);
            } catch (Exception e) {
            }
        }

        HeartBitServer.getInstance().stopService();
        logger.debug("HeartBitServer.getInstance().stopService() finished");
        HTTPMonitor.getInstance().stopService();
        logger.debug("HTTPMonitor.getInstance().stopService() finished");
        ReceivedPacketProcessor.getInstance().stopService();
        logger.debug("ReceivedPacketProcessor.getInstance().stopService() finished");
        ServerStatusProcessor.getInstance().stopService();
        logger.debug("ServerStatusProcessor.getInstance().stopService() finished");
        StatusUpdateProcessor.getInstance().stopService();
        logger.debug("StatusUpdateProcessor.getInstance().stopService() finished");

        if (Constants.SEND_STATISTICS) {
            ReloadQueueProcessor.getInstance().stopService();
            logger.debug("ReloadQueueProcessor.getInstance().stopService() finished");
            StatisticsSender.getInstance().stopService();
            logger.debug("StatisticsSender.getInstance().stopService() finished");
        }
        try {
            databaseconnector.DBConnector.getInstance().closeAllConnections();
            logger.debug("All DB connections are closed successfully");
        } catch (Exception ex) {
            logger.debug("Exception in closing all connections-->", ex);
        }

        System.out.println("HeartBeat Stopped at " + new java.util.Date());
        logger.debug("HeartBeatServer stopped at: " + new Date());
        ServerStatusChecker.setRunning(false);
        GaleraExecutorService.setRunning(false);
        System.exit(0);
    }
}
