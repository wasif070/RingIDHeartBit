/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.ndbmonitor;

import server.util.LoadConfig;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import server.dto.NDBServerDTO;
import server.util.Constants;
import server.util.SendSmsService;

/**
 *
 * @author USER
 */
public class ServerStatusChecker {

    private boolean isChecking = false;
    private static boolean isrunning;
    private final Logger LOGGER = Logger.getLogger(ServerStatusChecker.class.getName());

    public ServerStatusChecker() {
        setRunning(true);
    }

    public static void setRunning(boolean value) {
        isrunning = value;
    }

    public void checkNDBClusters() {
        if (!isChecking) {
            isChecking = true;
            try {
                if (LoadConfig.getInstance().getServersList().size() > 0) {
                    for (NDBServerDTO serverDTO : LoadConfig.getInstance().getServersList()) {
                        try {
                            long serverID = serverDTO.getId();
                            if (HelperMethods.isDead(serverID, serverDTO.getCluserGroup())) {
                                LOGGER.error(serverDTO.getName() + " (" + serverID + ":" + serverDTO.getIp() + ")=>  Status: Disconnected");
                                if (LoadConfig.getInstance().isSmsEnable()) {
                                    Thread.sleep(Constants.SECONDS_30);
                                    if (HelperMethods.isDead(serverID, serverDTO.getCluserGroup())) {
                                        String messageBody = makeSMSBody(serverID, serverDTO.getIp(), serverDTO.getName(), serverDTO.getCluserGroup());
                                        new SendSmsService(Constants.PHONE_NUMBERS, messageBody).start();
                                    }
                                }
                            } else {
                                LOGGER.info("Group:" + serverDTO.getCluserGroup() + " " + serverDTO.getName() + " (" + serverID + ":" + serverDTO.getIp() + ")" + " Status=> Connected");
                            }
                        } catch (IOException | InterruptedException e) {
                            LOGGER.error(e, e);
                        }
                    }
                }
            } catch (Exception e) {
                LOGGER.error(e, e);
            } finally {
                isChecking = false;
            }
        } else {
            LOGGER.info("Scheduler startted again but not checking. ");
        }
    }

    /**
     * This Scheduler will call checkNDBClusters method in time interval
     *
     * @see ServerStatusChecker#checkNDBClusters()
     */
    public void startChecking() {
        final ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        Runnable processDataCmd = () -> {
            try {
                checkNDBClusters();
                LOGGER.info("*****************Last execution time:" + new Date());
                if (!isrunning) {
                    LOGGER.error("stopThread : ReloadAll done ...");
                    service.shutdown();
                }
            } catch (Exception e) {
                LOGGER.error(e, e);
                setRunning(false);
                service.shutdown();
            } finally {
            }
        };
        service.scheduleAtFixedRate(processDataCmd, 1, 10, TimeUnit.MINUTES);
    }

    public void stopReloader() {
        setRunning(false);
        LOGGER.error("stopThread :stopReloader ...");
    }

    /**
     * Sample message : "Cluster group:1 'authproc1' '10.5.18.11' '101' is
     * Disconnected!"
     *
     * @param mobile
     * @return
     */
    private String makeSMSBody(long id, String ip, String serverName, long group) {
        StringBuilder bulder = new StringBuilder();
        bulder.append("'Cluster%20group:").append(group).append("'");
        bulder.append("%20'").append(serverName).append("'");
        bulder.append("%20'").append(ip).append("'");
        bulder.append("%20'").append(id).append("'");
        bulder.append("%20is%20Disconnected!");
        return bulder.toString();
    }

//    public static void main(String[] args) {
//        String messageBody = makeSMSBody(1, "12.3.5.6", "datanode3-auth9", 1);
//        String[] myIntArray = {"1728119927"};
//        new SendSmsService("NDBCluster", myIntArray, messageBody).start();
//    }
}
