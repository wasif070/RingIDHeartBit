/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.ndbmonitor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.log4j.Logger;
import server.util.Constants;

/**
 *
 * @author USER
 */
public class HelperMethods {

    private static final Logger LOGGER = Logger.getLogger(ServerStatusChecker.class.getName());

    /**
     * For cluster group =1
     * <p>
     * Main command is : ndb_mgm
     * <p>
     * For cluster group =2
     * <p>
     * Main command is : usr/local/bin/ndb_mgm 10.5.1.11 1187
     *
     * @param id
     * @param cmd
     * @return
     * @throws IOException
     */
    public static String executeNdbMgmCommand(long id, String[] cmd) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
//        String[] cmd = {
//            "ndb_mgm",
//            "--execute",
//            id + " status"
//        };
        Process p = Runtime.getRuntime().exec(cmd);
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String s2;
        while ((s2 = stdInput.readLine()) != null) {
            stringBuilder.append(s2).append(". ");
        }
        return stringBuilder.toString().trim();
    }

    public static boolean isDead(long id, long clusterGroup) throws IOException {
        String returnString = HelperMethods.executeNdbMgmCommand(id, getCommandArray(clusterGroup, id));
        if (returnString != null && returnString.trim().length() > 0) {
            if (returnString.contains("not")) {
                return true;
            }
        } else {
            LOGGER.error("Warning : Command execution failed for server id=>" + id);
        }
        return false;
    }

    /**
     * Command vary as cluster group
     *
     * @param clusterGroup
     * @param id
     * @return
     */
    public static String[] getCommandArray(long clusterGroup, long id) {
        if (clusterGroup == Constants.CLUSTER_GROUP.TWO) {
            return new String[]{"/usr/local/bin/ndb_mgm", "10.5.1.11" ,"1187", "--execute", id + " status"};
        } else {
            return new String[]{"ndb_mgm", "--execute", id + " status"};
        }
    }
}
