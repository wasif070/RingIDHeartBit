package server.db;

import databaseconnector.DBConnection;
import java.sql.SQLException;
import java.sql.Statement;
import server.util.Constants;
import org.apache.log4j.Logger;
import server.util.SMSSender;
import server.util.StorageQueue;

public class StatusUpdateProcessor extends Thread {

    private static StatusUpdateProcessor instance;
    private boolean running = false;
    private static final long PROCESSING_INTERVAL = 2000L;
    private static final Logger logger = Logger.getLogger(StatusUpdateProcessor.class.getName());

    public static StatusUpdateProcessor getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new StatusUpdateProcessor();
        }
    }

    public StatusUpdateProcessor() {
        running = true;
    }

    @Override
    public void run() {
        logger.debug("StatusUpdateProcessor is started.....");
        DBConnection dbConnection = null;
        Statement stmt = null;
        String sql = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();

            while (running) {
                try {
                    if (StorageQueue.getInstance().getIPPorts().size() > 20000) {
                        logger.debug("update queries " + StorageQueue.getInstance().getIPPorts().size());
                    }
                    if (dbConnection.connection == null || dbConnection.connection.isClosed()) {
                        try {
                            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                            stmt = dbConnection.connection.createStatement();
                        } catch (SQLException sqlEx) {
                        }
                    }
                    while (running && !StorageQueue.getInstance().getIPPorts().isEmpty()) {
//                        ServerDTO serverInfoDTO = null;
                        ServerDTO serverInfoDTO = StorageQueue.getInstance().getIPPorts().peek();
                        //String IPPort = StorageQueue.getInstance().getIPPorts().peek();

//                        if (ServersLoader.getInstance().containsKey(IPPort)) {
//                            serverInfoDTO = ServersLoader.getInstance().getServerDTO(IPPort);
//                        } else if (HTTPServersLoader.getInstance().isLive(IPPort)) {
//                            serverInfoDTO = HTTPServersLoader.getInstance().getLiveServerDTO(IPPort);
//                        } else if (HTTPServersLoader.getInstance().isDead(IPPort)) {
//                            serverInfoDTO = HTTPServersLoader.getInstance().getDeadServerDTO(IPPort);
//                        }
                        if (serverInfoDTO != null) {
                            switch (serverInfoDTO.getServerType()) {
                                case Constants.SERVER_TYPE_AUTH:
                                    sql = "update authservers set serverStatus=" + serverInfoDTO.getServerAliveStatus() + " where serverIP='" + serverInfoDTO.getServerIP() + "' and relayPort=" + serverInfoDTO.getServerPort();
                                    break;
                                case Constants.SERVER_TYPE_CHAT:
                                    sql = "update chatservers set serverStatus=" + serverInfoDTO.getServerAliveStatus() + " where serverIP='" + serverInfoDTO.getServerIP() + "' and registerPort=" + serverInfoDTO.getServerPort();
                                    break;
//                                case Constants.SERVER_TYPE_OFFLINE:
//                                    sql = "update offlineservers set serverStatus=" + serverInfoDTO.getServerAliveStatus() + " where serverIP='" + serverInfoDTO.getServerIP() + "' and offlinePort=" + serverInfoDTO.getServerPort();
//                                    break;
                                case Constants.SERVER_TYPE_VOICE:
                                    sql = "update voiceservers set serverStatus=" + serverInfoDTO.getServerAliveStatus() + " where serverIP='" + serverInfoDTO.getServerIP() + "' and registerPort=" + serverInfoDTO.getServerPort();
                                    break;
                                case Constants.SERVER_TYPE_AUTH_CONTROLLER:
                                    sql = "update authcontrollers set serverStatus=" + serverInfoDTO.getServerAliveStatus() + " where serverIP='" + serverInfoDTO.getServerIP() + "' and serverPort=" + serverInfoDTO.getServerPort();
                                    break;
                                case Constants.SERVER_TYPE_LOADBALANCER:
                                    sql = "update loadbalancers set serverStatus=" + serverInfoDTO.getServerAliveStatus() + " where serverIP='" + serverInfoDTO.getServerIP() + "' and serverPort=" + serverInfoDTO.getServerPort();
                                    break;
                                case Constants.SERVER_TYPE_LIVE_STREAM:
                                    sql = "update streamingservers set serverStatus=" + serverInfoDTO.getServerAliveStatus() + " where serverIP='" + serverInfoDTO.getServerIP() + "' and registerPort=" + serverInfoDTO.getServerPort();
                                    break;
                                case Constants.SERVER_TYPE_CHANNEL:
                                    sql = "update channelservers set serverStatus=" + serverInfoDTO.getServerAliveStatus() + " where serverIP='" + serverInfoDTO.getServerIP() + "' and heartbitPort=" + serverInfoDTO.getServerPort();
                                    break;
                                case Constants.SERVER_TYPE_COIN_SERVER:
                                    sql = "update coinservers set server_status=" + serverInfoDTO.getServerAliveStatus() + " where serverIP='" + serverInfoDTO.getServerIP() + "' and serverPort=" + serverInfoDTO.getServerPort();
                                    break;
                                case Constants.OFFLINE_MEADIA_STREAMING_SERVER:
                                    sql = "update streamingmediaservers set serverStatus=" + serverInfoDTO.getServerAliveStatus() + " where serverIP='" + serverInfoDTO.getServerIP() + "' and heartbitPort=" + serverInfoDTO.getServerPort();
                                    break;
                                case Constants.SERVER_TYPE_SMS_BD_SERVER:
                                    sql = "update smsbdservers set serverStatus=" + serverInfoDTO.getServerAliveStatus() + " where serverIP='" + serverInfoDTO.getServerIP() + "' and serverPort=" + serverInfoDTO.getServerPort();
                                    break;
                                default:
                                    break;
                            }
                            if (sql != null) {
                                logger.debug("Update SQL: " + sql);
                                if (stmt.executeUpdate(sql) > 0) {
                                    StorageQueue.getInstance().putForReload(serverInfoDTO);
                                    StorageQueue.getInstance().getIPPorts().poll();
                                    for (String phoneNumber : Constants.PHONE_NUMBERS) {
                                        SMSSender smsSender = new SMSSender(phoneNumber, Constants.TYPE_TO_NAME[serverInfoDTO.getServerType()] + " " + serverInfoDTO.getServerIP() + ":" + serverInfoDTO.getServerPort() + "'s status has chaged to " + Constants.LIVE_STATUS_STRING[serverInfoDTO.getServerAliveStatus()]);
                                        smsSender.start();
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Error in DBUpdateProcessor: " + sql, ex);
                }
                try {
                    if (running) {
                        Thread.sleep(PROCESSING_INTERVAL);
                    }
                } catch (Exception ex) {
                }
            }
        } catch (Exception ex) {
            logger.error("Error in DBUpdateProcessor01:", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    public void stopService() {
        running = false;
        this.interrupt();
    }
}
