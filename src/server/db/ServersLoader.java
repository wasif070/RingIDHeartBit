package server.db;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.log4j.Logger;
import server.util.Constants;

public class ServersLoader {

    private ConcurrentHashMap<String, ServerDTO> serversMap;
    private static ServersLoader instance = null;
    private static final Logger logger = Logger.getLogger(ServersLoader.class.getName());

    public ServersLoader() {
    }

    public static ServersLoader getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private static synchronized void createInstance() {
        if (instance == null) {
            instance = new ServersLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        serversMap = new ConcurrentHashMap<>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();

            for (int srvType = Constants.SERVER_TYPE_AUTH; srvType <= Constants.OFFLINE_MEADIA_STREAMING_SERVER; srvType++) {
                try {
                    stmt.setQueryTimeout(30);
                    String sql = null;
                    switch (srvType) {
                        case Constants.SERVER_TYPE_AUTH:
                            sql = "select serverIP as ip,relayPort as port,tcpPort from authservers where serverStatus=" + Constants.SERVER_STATUS_ALIVE;
                            break;
                        case Constants.SERVER_TYPE_CHAT:
                            sql = "select serverIP as ip,registerPort as port,0 as tcpPort from chatservers where serverStatus=" + Constants.SERVER_STATUS_ALIVE;
                            break;
//                        case Constants.SERVER_TYPE_OFFLINE:
//                            sql = "select serverIP as ip,offlinePort as port,0 as tcpPort  from offlineservers where serverStatus=" + Constants.SERVER_STATUS_ALIVE;
//                            break;
                        case Constants.SERVER_TYPE_VOICE:
                            sql = "select serverIP as ip,registerPort as port,0 as tcpPort from voiceservers where serverStatus=" + Constants.SERVER_STATUS_ALIVE;
                            break;
                        case Constants.SERVER_TYPE_LIVE_STREAM:
                            sql = "select serverIP as ip,registerPort as port,0 as tcpPort from streamingservers where serverStatus=" + Constants.SERVER_STATUS_ALIVE;
                            break;
                        case Constants.SERVER_TYPE_CHANNEL:
                            sql = "select serverIP as ip,heartbitPort as port,0 as tcpPort from channelservers where serverStatus=" + Constants.SERVER_STATUS_ALIVE;
                            break;
                        case Constants.OFFLINE_MEADIA_STREAMING_SERVER:
                            sql = "select serverIP as ip,heartbitPort as port,0 as tcpPort from streamingmediaservers where serverStatus=" + Constants.SERVER_STATUS_ALIVE;
                            break;
                        default:
                            break;
                    }
                    if (sql != null) {
                        resultSet = stmt.executeQuery(sql);
                        while (resultSet.next()) {
                            ServerDTO serverDTO = new ServerDTO();
                            serverDTO.setServerIP(resultSet.getString("ip"));
                            serverDTO.setServerPort(resultSet.getInt("port"));
                            serverDTO.setTcpPort(resultSet.getInt("tcpPort"));
                            serverDTO.setServerType(srvType);
                            serverDTO.setServerLastAliveTime(System.currentTimeMillis());
                            serverDTO.setServerAliveStatus(Constants.SERVER_STATUS_ALIVE);
                            serversMap.put(serverDTO.getServerIP() + ":" + serverDTO.getServerPort(), serverDTO);
                        }
                    }
                } catch (Exception e) {
                    logger.fatal("Exception in fetchServerInfoFromDB: ", e);
                }
            }
            logger.info("Servers are loaded successfully");
        } catch (Exception ex) {
            logger.fatal("DBException in fetchServerInfoFromDB: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    public synchronized void forceReload() {
        reload();
    }

    public ConcurrentHashMap<String, ServerDTO> getServersMap() {
        return serversMap;
    }

    public void putServerDTO(String key, ServerDTO serverDTO) {
        serversMap.put(key, serverDTO);
    }

    public ServerDTO getServerDTO(String key) {
        return serversMap.get(key);
    }

    public boolean containsKey(String key) {
        return serversMap.containsKey(key);
    }

    public int isValidRequest(String serverIP, int serverPort, int serverType) {
        int valid = 0;
        ServerDTO serverDTO = serversMap.get(serverIP + ":" + serverPort);
        if (serverDTO != null) {
            valid = 1 << 16;
            if (serverType == Constants.SERVER_TYPE_AUTH) {
                valid |= serverDTO.getTcpPort();
            }
            return valid;
        }

        DBConnection dbConnection = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String sql = null;
            switch (serverType) {
                case Constants.SERVER_TYPE_AUTH:
                    sql = "select id,tcpPort from authservers where serverIP='" + serverIP + "' and relayPort=" + serverPort;
                    break;
                case Constants.SERVER_TYPE_CHAT:
                    sql = "select id from chatservers where serverIP='" + serverIP + "' and registerPort=" + serverPort;
                    break;
//                case Constants.SERVER_TYPE_OFFLINE:
//                    sql = "select id from offlineservers where serverIP='" + serverIP + "' and offlinePort=" + serverPort;
//                    break;
                case Constants.SERVER_TYPE_VOICE:
                    sql = "select id from voiceservers where serverIP='" + serverIP + "' and registerPort=" + serverPort;
                    break;
                case Constants.SERVER_TYPE_LIVE_STREAM:
                    sql = "select id from streamingservers where serverIP='" + serverIP + "' and registerPort=" + serverPort;
                    break;
                case Constants.SERVER_TYPE_CHANNEL:
                    sql = "select id from channelservers where serverIP='" + serverIP + "' and heartbitPort=" + serverPort;
                    break;
                case Constants.OFFLINE_MEADIA_STREAMING_SERVER:
                    sql = "select id from streamingmediaservers where serverIP='" + serverIP + "' and heartbitPort=" + serverPort;
                    break;
                default:
                    break;
            }
            if (sql != null) {
                resultSet = stmt.executeQuery(sql);
                if (resultSet.next()) {
                    valid = 1 << 16;
                    if (serverType == Constants.SERVER_TYPE_AUTH) {
                        valid |= resultSet.getInt("tcpPort");
                    }
                }
            }
            logger.debug("Validity Check: serverIP=" + serverIP + ",Port=" + serverPort + ",Valid=" + (valid >>> 16));
        } catch (Exception e) {
            logger.fatal("Exception in isValidRequest: ", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return valid;
    }
}
