/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.db;

import databaseconnector.DBConnection;
import java.sql.Statement;
import org.apache.log4j.Logger;
import server.parser.Attributes;
import server.util.Constants;

/**
 *
 * @author Ashraful
 */
public class DBUpdater implements Runnable {

    private static final Logger logger = Logger.getLogger(DBUpdater.class.getName());
    private final int serverType;
    private final String IP;
    private final int port;
    private Attributes authAttributes;
    private Attributes voiceAttributes;
    private Attributes chatAttributes;
    private Attributes streamAttributes;
    private Attributes channelAttributes;
    private Attributes offlineMediaServerAttributes;

    public Attributes getAuthAttributes() {
        return authAttributes;
    }

    public void setAuthAttributes(Attributes authAttributes) {
        this.authAttributes = authAttributes;
    }

    public Attributes getVoiceAttributes() {
        return voiceAttributes;
    }

    public void setVoiceAttributes(Attributes voiceAttributes) {
        this.voiceAttributes = voiceAttributes;
    }

    public Attributes getChatAttributes() {
        return chatAttributes;
    }

    public void setChatAttributes(Attributes chatAttributes) {
        this.chatAttributes = chatAttributes;
    }

    public Attributes getStreamAttributes() {
        return streamAttributes;
    }

    public void setStreamAttributes(Attributes streamAttributes) {
        this.streamAttributes = streamAttributes;
    }

    public Attributes getChannelAttributes() {
        return channelAttributes;
    }

    public void setChannelAttributes(Attributes channelAttributes) {
        this.channelAttributes = channelAttributes;
    }

    public Attributes getOfflineMediaServerAttributes() {
        return offlineMediaServerAttributes;
    }

    public void setOfflineMediaServerAttributes(Attributes offlineMediaServerAttributes) {
        this.offlineMediaServerAttributes = offlineMediaServerAttributes;
    }

    public DBUpdater(int serverType, String IP, int port) {
        this.serverType = serverType;
        this.IP = IP;
        this.port = port;
    }

    public void run() {
        DBConnection dbConnection = null;
        Statement stmt = null;
        String sql = null;
        try {
            switch (serverType) {
                case Constants.SERVER_TYPE_AUTH: {
                    sql = "update authservers set currentRegitration=" + authAttributes.getCurrentRegistration() + ",liveSessions=" + authAttributes.getLiveSessions() + " where serverIP='" + IP + "' and relayPort=" + port;
                    break;
                }
                case Constants.SERVER_TYPE_VOICE: {
                    sql = "update voiceservers set currentRegitration=" + voiceAttributes.getCurrentRegistration() + ",p2pSessions=" + voiceAttributes.getLiveSessions() + " where serverIP='" + IP + "' and registerPort=" + port;
                    break;
                }
                case Constants.SERVER_TYPE_CHAT: {
                    sql = "update chatservers set currentRegitration=" + chatAttributes.getCurrentRegistration() + " where serverIP='" + IP + "' and registerPort=" + port;
                    break;
                }
                case Constants.SERVER_TYPE_LIVE_STREAM: {
                    sql = "update streamingservers set currentRegistration=" + streamAttributes.getCurrentRegistration() + ",noOfPublisher=" + streamAttributes.getNoOfPublisher() + " where serverIP='" + IP + "' and registerPort=" + port;
                    break;
                }
                case Constants.SERVER_TYPE_CHANNEL: {
                    sql = "update channelservers set currentRegistration=" + channelAttributes.getCurrentRegistration() + " where serverIP='" + IP + "' and heartbitPort=" + port;
                    break;
                }
                case Constants.OFFLINE_MEADIA_STREAMING_SERVER: {
                    sql = "update streamingmediaservers set currentRegistration=" + offlineMediaServerAttributes.getCurrentRegistration() + " where serverIP='" + IP + "' and heartbitPort=" + port;
                    break;
                }
                default:
                    break;
            }
            if (sql != null) {
                logger.debug("Value Update SQL: " + sql);
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                stmt = dbConnection.connection.createStatement();
                stmt.executeUpdate(sql);
            }
        } catch (Exception ex) {
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }
}
