package server.db;

import server.util.Constants;

public class ServerDTO {

    private int serverAliveStatus = Constants.SERVER_STATUS_DEAD;
    private int serverPort = 0;
    private int serverType = 0;
    private long serverLastAliveTime;
    private String serverIP;
    private int tcpPort = 0;
    private long messageSentTime;
    private int messageSentCount;

    public ServerDTO() {
    }

    public int getServerAliveStatus() {
        return serverAliveStatus;
    }

    public void setServerAliveStatus(int serverAliveStatus) {
        this.serverAliveStatus = serverAliveStatus;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getServerType() {
        return serverType;
    }

    public void setServerType(int serverType) {
        this.serverType = serverType;
    }

    public long getServerLastAliveTime() {
        return serverLastAliveTime;
    }

    public void setServerLastAliveTime(long serverLastAliveTime) {
        this.serverLastAliveTime = serverLastAliveTime;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public int getTcpPort() {
        return tcpPort;
    }

    public void setTcpPort(int tcpPort) {
        this.tcpPort = tcpPort;
    }

    public long getMessageSentTime() {
        return messageSentTime;
    }

    public void setMessageSentTime(long messageSentTime) {
        this.messageSentTime = messageSentTime;
    }

    public int getMessageSentCount() {
        return messageSentCount;
    }

    public void setMessageSentCount(int messageSentCount) {
        this.messageSentCount = messageSentCount;
    }
}
