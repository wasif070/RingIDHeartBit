package server.notify;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import server.http.HTTPClient;
import server.http.HTTPSClient;
import server.http.HTTPServersLoader;

public class AuthControllerNotifier {

    private static final Logger logger = Logger.getLogger(AuthControllerNotifier.class.getName());

    public static AuthControllerNotifier getInstance() {
        return AuthControllerNotifier_SINGLETON_HOLDER.INSTANCE;
    }

    private static class AuthControllerNotifier_SINGLETON_HOLDER {

        private static final AuthControllerNotifier INSTANCE = new AuthControllerNotifier();
    }

    public int reload(int reloadCase) {
        String completeUrl;
        int responseCode, sucsCount = 0;
        ArrayList<String> authControllerUrls = HTTPServersLoader.getInstance().getAuthControllerUrls();
        int loopCount = 1;
        logger.debug("authControllerUrls -> " + authControllerUrls.size());
        for (String url : authControllerUrls) {;
            completeUrl = new StringBuilder(url).append("/common/ForceReload?rt=").append(reloadCase).toString();
            logger.debug("<reloadCase> " + reloadCase + " : <" + loopCount + "> completeUrl -> " + completeUrl);
            try {
                if (completeUrl.startsWith("https")) {
                    HTTPSClient httpClient = new HTTPSClient(completeUrl, "");
                    httpClient.visitUrl("GET");
                    responseCode = httpClient.getResponseCode();
                    httpClient.finish();
                } else {
                    HTTPClient httpClient = new HTTPClient(completeUrl);
                    responseCode = httpClient.getResponseCode();
                    httpClient.finish();
                }
                if (responseCode == 200) {
                    sucsCount++;
                }
                logger.debug("<" + loopCount + "> responseCode : " + responseCode);
            } catch (Exception e) {
                logger.error("AuthControllerNotifier Exception : ", e);
            }
        }
        return sucsCount;
    }

    public static void main(String[] args) {
        getInstance().reload(5);
    }
}
