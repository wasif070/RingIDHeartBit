/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.parser;

/**
 *
 * @author Ashraful
 */
public class PacketParser {

    public static Attributes parsePacket(byte[] bytes, int offset) {
        Attributes attributes = new Attributes();
        for (int index = offset; index < bytes.length;) {
            int attribute = (bytes[index++] & 0xFF);
            int length = 0;
            switch (attribute) {
                case AttributeCodes.CURRENT_REGISTRATION: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setCurrentRegistration(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.LIVE_SESSIONS: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setLiveSessions(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.NUMBER_OF_PUBLISHER: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setNoOfPublisher(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.ACTION: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setAction(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.NUMBER_OF_EXCEPTIONS: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setAction(getInt(bytes, index, length));
                    break;
                }
            }
            index += length;
        }
        return attributes;
    }

    private static int getInt(byte[] bytes, int index, int length) {
        int result = 0;
        for (int i = (length - 1); i > -1; i--) {
            result |= (bytes[index++] & 0xFF) << (8 * i);
        }
        return result;
    }
}
