/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.parser;

/**
 *
 * @author Ashraful
 */
public class PacketBuilder {

    public static int addShort(int attribute, Integer val, int length, byte[] send_bytes, int index) {
        if (val == null || val.intValue() == 0) {
            return index;
        }
        int value = val.intValue();

        send_bytes[index++] = (byte) (attribute);
        send_bytes[index++] = (byte) (length);

        send_bytes[index++] = (byte) (value >> 8);
        send_bytes[index++] = (byte) (value);

        return index;
    }

    public static int addInt(int attribute, Integer val, int length, byte[] send_bytes, int index) {
        if (val == null || val.intValue() == 0) {
            return index;
        }
        int value = val.intValue();

        send_bytes[index++] = (byte) (attribute);
        send_bytes[index++] = (byte) (length);

        send_bytes[index++] = (byte) (value >> 24);
        send_bytes[index++] = (byte) (value >> 16);
        send_bytes[index++] = (byte) (value >> 8);
        send_bytes[index++] = (byte) (value);

        return index;
    }

    public static int addLong(int attribute, Long val, int length, byte[] send_bytes, int index) {
        if (val == null || val.longValue() == 0) {
            return index;
        }
        long value = val.longValue();

        send_bytes[index++] = (byte) attribute;
        send_bytes[index++] = (byte) length;

        send_bytes[index++] = (byte) (value >> 56);
        send_bytes[index++] = (byte) (value >> 48);
        send_bytes[index++] = (byte) (value >> 40);
        send_bytes[index++] = (byte) (value >> 32);
        send_bytes[index++] = (byte) (value >> 24);
        send_bytes[index++] = (byte) (value >> 16);
        send_bytes[index++] = (byte) (value >> 8);
        send_bytes[index++] = (byte) (value);

        return index;
    }

    public static int addString(int attribute, String value, byte[] send_bytes, int index) {
        if (value == null || value.length() == 0) {
            return index;
        }
        send_bytes[index++] = (byte) attribute;

        byte[] str_bytes = value.getBytes();
        int length = str_bytes.length;
        send_bytes[index++] = (byte) length;

        System.arraycopy(str_bytes, 0, send_bytes, index, length);
        index += length;

        return index;
    }

    public static int addBool(int attribute, boolean value, int length, byte[] send_bytes, int index) {
        send_bytes[index++] = (byte) (attribute);

        send_bytes[index++] = (byte) (length);
        send_bytes[index++] = (byte) (value ? 1 : 0);

        return index;
    }
}
