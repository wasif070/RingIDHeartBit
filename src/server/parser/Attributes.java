/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.parser;

/**
 *
 * @author Ashraful
 */
public class Attributes {

    private int currentRegistration;
    private int liveSessions;
    private int noOfPublisher;
    private int action;

    public int getCurrentRegistration() {
        return currentRegistration;
    }

    public void setCurrentRegistration(int currentRegistration) {
        this.currentRegistration = currentRegistration;
    }

    public int getLiveSessions() {
        return liveSessions;
    }

    public void setLiveSessions(int liveSessions) {
        this.liveSessions = liveSessions;
    }

    public int getNoOfPublisher() {
        return noOfPublisher;
    }

    public void setNoOfPublisher(int noOfPublisher) {
        this.noOfPublisher = noOfPublisher;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }
}
