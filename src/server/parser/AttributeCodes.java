package server.parser;

/**
 *
 * @author Ashraful
 */
public class AttributeCodes {

    public static final int CURRENT_REGISTRATION = 1;
    public static final int LIVE_SESSIONS = 2;
    public static final int NUMBER_OF_PUBLISHER = 3;
    public static final int NUMBER_OF_EXCEPTIONS = 4;
    /*Admiinistrative*/
    public static final int ACTION = 100;
}
