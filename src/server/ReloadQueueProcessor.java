package server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.log4j.Logger;
import server.db.ServersLoader;
import server.db.ServerDTO;
import server.http.HTTPClient;
import server.http.HTTPServerDTO;
import server.http.HTTPServersLoader;
import server.http.TCPClient;
import server.util.Constants;
import server.util.StorageQueue;

public class ReloadQueueProcessor extends Thread {

    private static ReloadQueueProcessor instance;
    private boolean running = false;
    private static final long SLEEP_INTERVAL = 10000L;
    private static final Logger logger = Logger.getLogger(ReloadQueueProcessor.class.getName());

    public static ReloadQueueProcessor getInstance() {
        if (instance == null) {
            instance = new ReloadQueueProcessor();
        }
        return instance;
    }

    public ReloadQueueProcessor() {
        running = true;
    }

    @Override
    public void run() {
        logger.debug("ReloadQueueProcessor is started.....");
        ConcurrentLinkedQueue<ServerDTO> reloadQueue;
        ServerDTO serverDTO;
        while (running) {
            try {
                reloadQueue = StorageQueue.getInstance().getReloadQueue();
                while (running && !reloadQueue.isEmpty()) {
                    try {
                        serverDTO = reloadQueue.poll();
                        logger.info("Reloading server[" + serverDTO.getServerIP() + ":" + serverDTO.getServerPort() + ", type " + serverDTO.getServerType() + "] status " + serverDTO.getServerAliveStatus());
                        switch (serverDTO.getServerType()) {
                            case Constants.SERVER_TYPE_AUTH: {
                                reloadAuthServers("AuthServer", "reloadauth");
                                reloadAuthControllers("/rac/reloadcomports");
                                break;
                            }
                            case Constants.SERVER_TYPE_CHAT: {
                                reloadAuthServers("ChatServer", "reload-chat-servers");
                                break;
                            }
                            case Constants.SERVER_TYPE_VOICE: {
                                reloadAuthServers("VoiceServer", "reload-voice-servers");
                                break;
                            }
//                            case Constants.SERVER_TYPE_OFFLINE: {
//                                reloadServer(serverDTO, "OfflineServer", Constants.SERVER_TYPE_OFFLINE, "reload-offline-servers");
//                                break;
//                            }
                            case Constants.SERVER_TYPE_LIVE_STREAM: {
                                reloadAuthServers("LiveStreamServer", "reload-streaming-servers");
                                break;
                            }
                            case Constants.SERVER_TYPE_CHANNEL: {
                                reloadAuthServers("ChannelServer", "reload-channel-servers");
                                break;
                            }
                            case Constants.SERVER_TYPE_COIN_SERVER: {
                                reloadAuthServers("CoinServer", "reload-coin-servers");
                                break;
                            }
                            case Constants.OFFLINE_MEADIA_STREAMING_SERVER: {
                                reloadAuthServers("MeadiaStreamingServer", "reload-streaming-media-servers");
                                break;
                            }
                            case Constants.SERVER_TYPE_RINGBIT_SERVER: {
                                reloadAuthServers("RingbitServer", "reload-ringbit-servers");
                                break;
                            }
                            case Constants.SERVER_TYPE_SMS_BD_SERVER: {
//                                AuthControllerNotifier.getInstance().reload(Constants.AuthcontrollerReloadCase.SMSBD);
                                reloadAuthControllers("/common/ForceReload?rt=" + Constants.AuthcontrollerReloadCase.SMSBD);
                                break;
                            }
                        }
                    } catch (NullPointerException ex) {
                        logger.fatal("Error in ReloadQueueProcessor inner while ", ex);
                    }
                }
            } catch (NullPointerException ex) {
                logger.fatal("Error in ReloadQueueProcessor outer while ", ex);
            }
            try {
                if (running) {
                    Thread.sleep(SLEEP_INTERVAL);
                }
            } catch (Exception ex) {
            }
        }
    }

    private boolean reloadAuthServers(String serverName, String URL) {
        boolean result = true;
        for (Map.Entry<String, ServerDTO> entry : ServersLoader.getInstance().getServersMap().entrySet()) {
            try {
                ServerDTO server = entry.getValue();
                if (server != null && server.getServerType() == Constants.SERVER_TYPE_AUTH && server.getServerAliveStatus() == Constants.SERVER_STATUS_ALIVE) {
                    String response = TCPClient.sendRequest(InetAddress.getByName(server.getServerIP()), server.getTcpPort(), URL);
                    if (response == null || !response.equals("true")) {
                        result = false;
                    }
                    logger.info(serverName + " [" + server.getServerIP() + ":" + server.getTcpPort() + "] reload response : " + response);
                    try {
                        sleep(250);
                    } catch (InterruptedException ex) {
                    }
                }
            } catch (NullPointerException | UnknownHostException ex) {
                logger.error("Error in reloadServer " + URL + "->", ex);
            }
        }
        return result;
    }

    private boolean reloadAuthControllers(String URL) {
        boolean result = true;
        for (Map.Entry<String, HTTPServerDTO> entry : HTTPServersLoader.getInstance().getLiveServersMapHTTP().entrySet()) {
            try {
                HTTPServerDTO httpServer = entry.getValue();
                if (httpServer != null && httpServer.getServerType() == Constants.SERVER_TYPE_AUTH_CONTROLLER && httpServer.getServerAliveStatus() == Constants.SERVER_STATUS_ALIVE) {
                    String controllerURL = "http://" + httpServer.getServerIP() + (httpServer.getServerPort() > 0 ? (":" + httpServer.getServerPort()) : "") + URL;
                    HTTPClient httpClient = new HTTPClient(controllerURL);
                    logger.info("Auth controller[" + controllerURL + "] reload response code : " + httpClient.getResponseCode());
                    if (httpClient.getResponseCode() != 200) {
                        result = false;
                    }
                    httpClient.finish();
                    try {
                        sleep(250);
                    } catch (InterruptedException ex) {
                    }
                }
            } catch (NullPointerException | IOException ioEx) {
                logger.error("Error in reloadAuthControllers " + URL + "->", ioEx);
            }
        }
        return result;
    }

    public void stopService() {
        running = false;
        this.interrupt();
    }
}
