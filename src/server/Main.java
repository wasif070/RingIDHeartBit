package server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import server.db.ServersLoader;
import server.db.StatusUpdateProcessor;
import server.galera.GaleraConstants;
import server.galera.GaleraExecutorService;
import server.http.HTTPMonitor;
import server.util.LoadConfig;
import server.ndbmonitor.ServerStatusChecker;
import server.util.Constants;
import server.util.StatisticsSender;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ashraful
 */
public class Main {

    private static int BIND_PORT = 0;
    private static String BIND_IP = null;
    private static final String CONFIG = "config.txt";
    private static ShutdownDetection shutdownDetection = null;
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String args[]) {
        try {
            PropertyConfigurator.configure("log4j.properties");
            logger.debug("------------ Starting heartbit server ---------------");
            shutdownDetection = new ShutdownDetection();
            shutdownDetection.start();
            try {
                InputStream input = null;
                File file = new File(CONFIG);
                if (file.exists()) {
                    input = new FileInputStream(file);
                }
                Properties prop = new Properties();
                prop.load(input);
                input.close();
                if (prop.containsKey(Constants.BIND_IP)) {
                    BIND_IP = prop.getProperty(Constants.BIND_IP);
                } else {
                    logger.debug("No " + Constants.BIND_IP + " is found in config.txt");
                }
                if (prop.containsKey(Constants.BIND_PORT)) {
                    BIND_PORT = Integer.parseInt(prop.getProperty(Constants.BIND_PORT));
                } else {
                    logger.debug("No " + Constants.BIND_PORT + " is found in config.txt");
                }

                if (prop.containsKey("sms_url")) {
                    Constants.SMS_URL = prop.getProperty("sms_url");
                    logger.info("SMS URL-->" + Constants.SMS_URL);
                } else {
                    logger.info("sms_url is not found in config file.");
                }

                if (prop.containsKey("brand_id")) {
                    Constants.BRAND_ID = prop.getProperty("brand_id");
                    logger.info("BRAND_ID-->" + Constants.BRAND_ID);
                } else {
                    logger.info("BRAND_ID is not found in config file.");
                }

                if (prop.containsKey("send_stat")) {
                    Constants.SEND_STATISTICS = Boolean.parseBoolean(prop.getProperty("send_stat"));
                    logger.info("SEND_STATISTICS-->" + Constants.SEND_STATISTICS);
                } else {
                    logger.info("SEND_STATISTICS is not found in config file.");
                }

                if (prop.containsKey("phone_number")) {
                    String phoneNumberString = prop.getProperty("phone_number");
                    if (phoneNumberString != null && phoneNumberString.trim().length() > 0) {
                        String[] phoneNumbers = phoneNumberString.split(",");
                        Constants.PHONE_NUMBERS = new String[phoneNumbers.length];

                        System.arraycopy(phoneNumbers, 0, Constants.PHONE_NUMBERS, 0, phoneNumbers.length);
                    }
                    logger.info("Phone number-->" + phoneNumberString);
                } else {
                    logger.info("phone_number is not found in config file.");
                }

                if (prop.containsKey("phone_number_special")) {
                    String phoneNumberString = prop.getProperty("phone_number_special");
                    if (phoneNumberString != null && phoneNumberString.trim().length() > 0) {
                        String[] phoneNumbers = phoneNumberString.split(",");
                        Constants.PHONE_NUMBERS_SPECIAL = new String[phoneNumbers.length];

                        System.arraycopy(phoneNumbers, 0, Constants.PHONE_NUMBERS_SPECIAL, 0, phoneNumbers.length);
                    }
                    logger.info("Special phone number-->" + phoneNumberString);
                } else {
                    logger.info("phone_number_special is not found in config file.");
                }

                if (prop.containsKey("heavy_log")) {
                    Constants.HEAVY_LOG_ENABLE = Boolean.parseBoolean(prop.getProperty("heavy_log"));
                    logger.info("heavy_log-->" + Constants.HEAVY_LOG_ENABLE);
                } else {
                    logger.info("HEAVY_LOG_ENABLE is not found in config file.");
                }
            } catch (IOException | NullPointerException | NumberFormatException e) {
                logger.fatal("Exception in reading config file:" + e);
            }
            LoadConfig.getInstance().loadData();
            ServersLoader.getInstance().forceReload();
            ReceivedPacketProcessor.getInstance().start();
            ServerStatusProcessor.getInstance().start();
            StatusUpdateProcessor.getInstance().start();

            if (Constants.SEND_STATISTICS) {
                ReloadQueueProcessor.getInstance().start();
                StatisticsSender.getInstance().start();
            }

            HeartBitServer.getInstance().init(BIND_IP, BIND_PORT);
            HeartBitServer.getInstance().start();
            HTTPMonitor.getInstance().start();

            runNDBClusterMonitor();
            runGaleraMonitor();
            System.out.println("HeartBeat Server started at " + new java.util.Date());
            logger.debug("HeartBeat Server started at " + new java.util.Date());
        } catch (Exception e) {
            logger.fatal("ServerAliveProcessor Exception: ", e);
            System.exit(1);
        }
    }

    private static void runNDBClusterMonitor() {
        ServerStatusChecker checker = new ServerStatusChecker();
        checker.startChecking();
        logger.info("runNDBClusterMonitor started=>" + new Date());
    }

    private static void runGaleraMonitor() {
        try {
            for (String server : GaleraConstants.galeraServers) {
                GaleraExecutorService executorService = new GaleraExecutorService(GaleraConstants.APP_STRING, server, GaleraConstants.userName, GaleraConstants.password, GaleraConstants.phoneNumbers);
                executorService.startSchedulerService();
            }
        } catch (Exception e) {
        }
    }
}
