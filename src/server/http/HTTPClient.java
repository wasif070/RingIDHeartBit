/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rejuan
 */
public class HTTPClient {
    
    private int port;
    private String ip;
    private String path;
    private Socket socket;
    private int responseCode;
    
    public HTTPClient(String link) throws MalformedURLException, IOException {
        responseCode = 0;
        this.parseURL(link);
    }
    
    private void parseURL(String link) throws MalformedURLException, IOException {
        
        URL url = new URL(link);
        ip = url.getHost();
        
        port = url.getPort();
        if (port == -1) {
            port = 80;
        }
        
        path = url.getPath();
        String query = url.getQuery();
        if ((query != null) && (query.trim().length() > 0)) {
            path = path + "?" + query;
        }
        
        this.visitUrl();
    }
    
    private void visitUrl() throws IOException {
        
        socket = new Socket(InetAddress.getByName(ip), port);
        socket.setSoTimeout(30000);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        OutputStream outputStream = socket.getOutputStream();
        
        String request;
        
        if (port == 80) {
            request = "GET " + path + " HTTP/1.1\r\nHost: " + ip + "\r\nConnection: close\r\n\r\n";
        } else {
            request = "GET " + path + " HTTP/1.1\r\nHost: " + ip + ":" + port + "\r\nConnection: close\r\n\r\n";
        }
        
        outputStream.write(request.getBytes());
        outputStream.flush();
        
        String responseFirstLine = bufferedReader.readLine().trim();
        String[] httpHeaders = responseFirstLine.split(" ");
        
        if (httpHeaders.length >= 2) {
            try {
                responseCode = Integer.parseInt(httpHeaders[1]);
            } catch (NumberFormatException e) {
            }
        }
        
        bufferedReader.close();
        outputStream.close();
    }
    
    public int getResponseCode() {
        return this.responseCode;
    }
    
    public void finish() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }public static void main(String[] args) {
        HTTPClient httpc;
        try {
//            httpc = new HTTPClient("http://devcoin.ringid.com:8024/investmentcoin/api/wallet/123456789");
//            httpc = new HTTPClient("http://devcoin.ringid.com:8077/api/userwallet/getuserwalletstatus?userId=123456789");
            httpc = new HTTPClient("http://103.26.115.150/");
            System.out.println(""+httpc.getResponseCode());
        } catch (IOException ex) {
            Logger.getLogger(HTTPClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
