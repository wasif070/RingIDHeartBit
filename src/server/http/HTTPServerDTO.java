package server.http;

import server.db.ServerDTO;

public class HTTPServerDTO extends ServerDTO {

    private String serverURL;
    private String IPorURL;
    private int attemptNumber = 0;
    private String loadbalancerType;
    private String host;
    private boolean secured = false;

    public HTTPServerDTO() {
    }

    public String getServerURL() {
        return serverURL;
    }

    public void setServerURL(String serverURL) {
        this.serverURL = serverURL;
    }

    public int getAttemptNumber() {
        return attemptNumber;
    }

    public void setAttemptNumber(int attemptNumber) {
        this.attemptNumber = attemptNumber;
    }

    public String getLoadbalancerType() {
        return loadbalancerType;
    }

    public void setLoadbalancerType(String loadbalancerType) {
        this.loadbalancerType = loadbalancerType;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public boolean isSecured() {
        return secured;
    }

    public void setSecured(boolean secured) {
        this.secured = secured;
    }

    public String getIPorURL() {
        return IPorURL;
    }

    public void setIPorURL(String IPorURL) {
        this.IPorURL = IPorURL;
    }

}
