/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.http;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.log4j.Logger;

/**
 *
 * @author rejuan
 */
public class HTTPSClient {

    private final String link;
    private final String host;
    private String response;
    private Socket socket;
    private int responseCode;
    private static Logger logger = Logger.getLogger(HTTPSClient.class.getName());
    private static final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        @Override
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        @Override
        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }

        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
    }
    };
    private static final HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public HTTPSClient(String link, String host) throws MalformedURLException, IOException {
        this.link = link;
        this.host = host;
    }

    public void visitUrl(String method) throws IOException {
        HttpsURLConnection conn = null;
        try {
            System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            conn = (HttpsURLConnection) new URL(link).openConnection();
            conn.setRequestMethod(method);
            if (host != null && host.length() > 0) {
                conn.setRequestProperty("Host", host);
            }

            responseCode = conn.getResponseCode();

            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuilder.append(line);
            }
            rd.close();
            response = stringBuilder.toString();

        } catch (Exception e) {
            logger.debug("HTTPSClient Exception for link " + link + ", host " + host + " --> ", e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public String getResponse() {
        return response;
    }

    public int getResponseCode() {
        return this.responseCode;
    }

    public void finish() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }

    public static void main(String[] args) {
        HTTPSClient hTTPSClient = null;
        try {
//            hTTPSClient = new HTTPSClient("https://104.193.36.228/rac/comports?lt=1&ringID=2110063638");
            hTTPSClient = new HTTPSClient("https://104.193.36.228/", "auth.ringid.com");
            hTTPSClient.visitUrl("GET");
            if (hTTPSClient.getResponseCode() == 200 && hTTPSClient.getResponse().length() > 0) {
                System.out.println("200 OK");
            } else {
                System.err.println("hTTPSClient.getResponseCode() --> " + hTTPSClient.getResponseCode());
            }
        } catch (Exception e) {
        }
    }
}
