package server.http;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.log4j.Logger;
import server.util.Constants;

public class HTTPServersLoader {

    public ConcurrentHashMap<String, HTTPServerDTO> liveServersMapHTTP;
    public ConcurrentHashMap<String, HTTPServerDTO> deadServersMapHTTP;
    private static HTTPServersLoader instance = null;
    private static final Logger logger = Logger.getLogger(HTTPServersLoader.class.getName());
    private long updateTime = 0L;
    private ArrayList<String> authControllerUrls;

    public static HTTPServersLoader getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private static synchronized void createInstance() {
        if (instance == null) {
            instance = new HTTPServersLoader();
        }
    }

    public HTTPServersLoader() {
        forceReload();
    }

    public boolean isLive(String key) {
        return liveServersMapHTTP.containsKey(key);
    }

    public HTTPServerDTO getLiveServerDTO(String key) {
        return liveServersMapHTTP.get(key);
    }

    public HTTPServerDTO getDeadServerDTO(String key) {
        return deadServersMapHTTP.get(key);
    }

    public boolean isDead(String key) {
        return deadServersMapHTTP.containsKey(key);
    }

    public ConcurrentHashMap<String, HTTPServerDTO> getDeadServersMapHTTP() {
        return deadServersMapHTTP;
    }

    public ConcurrentHashMap<String, HTTPServerDTO> getLiveServersMapHTTP() {
        return liveServersMapHTTP;
    }

    public void checkForReload() {
        if (System.currentTimeMillis() - updateTime > Constants.MINUTES_30) {
            forceReload();
        }
    }

    public synchronized final void forceReload() {
        DBConnection dbConnection = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        liveServersMapHTTP = new ConcurrentHashMap<>();
        deadServersMapHTTP = new ConcurrentHashMap<>();
        boolean noError = true;
        try {
            ArrayList<String> authControllerUrlsLocal = new ArrayList<>();
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();

            Integer[] servers = {Constants.SERVER_TYPE_AUTH_CONTROLLER, Constants.SERVER_TYPE_LOADBALANCER, Constants.SERVER_TYPE_COIN_SERVER, Constants.SERVER_TYPE_RINGBIT_SERVER, Constants.SERVER_TYPE_SMS_BD_SERVER};
            for (int srvType : servers) {
                try {
                    stmt.setQueryTimeout(30);
                    String sql = null;
                    String urlSuffix = "";
                    String host = "";
                    switch (srvType) {
                        case Constants.SERVER_TYPE_AUTH_CONTROLLER:
                            sql = "select serverIP as ip,serverPort as port,serverStatus as status, 0 as secure  from authcontrollers";
                            urlSuffix = Constants.AUTH_CONTROLLER_SUFFIX;
                            break;
                        case Constants.SERVER_TYPE_LOADBALANCER:
                            sql = "select serverIP as ip,serverPort as port, loadBalancerType,serverStatus as status, isSecure as secure from loadbalancers";
                            break;
                        case Constants.SERVER_TYPE_COIN_SERVER:
                            sql = "select serverIP as ip,serverPort as port,server_status as status, 0 as secure from coinservers";
                            urlSuffix = Constants.COIN_SERVER_SUFFIX;
                            break;
                        case Constants.SERVER_TYPE_RINGBIT_SERVER:
                            sql = "select serverIP as ip,serverPort as port,server_status as status, 0 as secure from ringbitservers";
                            urlSuffix = Constants.RINGBIT_SERVER_SUFFIX;
                            break;
                        case Constants.SERVER_TYPE_SMS_BD_SERVER:
                            sql = "select serverIP as ip,serverPort as port,serverStatus as status, 0 as secure from smsbdservers";
                            urlSuffix = Constants.SMSBD_SERVER_SUFFIX;
                            break;
                        default:
                            break;
                    }

                    if (sql != null) {
                        resultSet = stmt.executeQuery(sql);
                        while (resultSet.next()) {
                            try {
                                HTTPServerDTO httpServerDTO = new HTTPServerDTO();
                                httpServerDTO.setServerIP(resultSet.getString("ip"));
                                httpServerDTO.setServerPort(resultSet.getInt("port"));
                                httpServerDTO.setServerType(srvType);
                                httpServerDTO.setServerLastAliveTime(System.currentTimeMillis());
                                httpServerDTO.setServerAliveStatus(Constants.SERVER_STATUS_ALIVE);

                                if (srvType == Constants.SERVER_TYPE_LOADBALANCER) {
                                    String loadbalancerType = resultSet.getString("loadBalancerType");
                                    if (loadbalancerType == null) {
                                        throw new Exception("Loadbalancer Type cann't be empty");
                                    }
                                    switch (loadbalancerType) {
                                        case Constants.LOADBALANCER_TYPE_AUTH_CONTROLLER:
                                            urlSuffix = Constants.AUTH_CONTROLLER_SUFFIX;
                                            host = Constants.LOADBALANCER_HOST_AUTH_DOT_RINGID_DOT_COM;
                                            break;
                                        case Constants.LOADBALANCER_TYPE_CLOUD:
                                            urlSuffix = Constants.CLOUD_SUFFIX;
                                            host = Constants.LOADBALANCER_HOST_IMAGES_DOT_RINGID_DOT_COM;
                                            break;
                                        case Constants.LOADBALANCER_TYPE_MOB_WEB:
                                            urlSuffix = "";
                                            host = Constants.LOADBALANCER_HOST_MEDIA_DOT_RINGID_DOT_COM;
                                            break;
                                        case Constants.LOADBALANCER_TYPE_WEB:
                                            urlSuffix = "";
                                            host = Constants.LOADBALANCER_HOST_RINGID_DOT_COM;
                                            break;
                                        default:
                                            throw new Exception("Unknown loadbalancerType : " + loadbalancerType);
                                    }
                                    httpServerDTO.setLoadbalancerType(loadbalancerType);
                                } else if (srvType == Constants.SERVER_TYPE_AUTH_CONTROLLER) {
                                    authControllerUrlsLocal.add(new StringBuilder((httpServerDTO.isSecured() ? "https://" : "http://")).append(httpServerDTO.getServerIP()).append(":").append(httpServerDTO.getServerPort()).toString());
                                }

                                httpServerDTO.setSecured(resultSet.getInt("secure") == 1);
                                httpServerDTO.setHost(host);
                                httpServerDTO.setServerURL((httpServerDTO.isSecured() ? "https://" : "http://") + resultSet.getString("ip") + (httpServerDTO.getServerPort() > 0 ? ":" + httpServerDTO.getServerPort() : "") + urlSuffix);

                                int serverStatus = resultSet.getInt("status");
                                if (serverStatus == 1) {
                                    liveServersMapHTTP.put(httpServerDTO.getServerIP() + ":" + httpServerDTO.getServerPort(), httpServerDTO);
                                } else {
                                    deadServersMapHTTP.put(httpServerDTO.getServerIP() + ":" + httpServerDTO.getServerPort(), httpServerDTO);
                                }
                            } catch (Exception e) {
                                noError = false;
                                logger.error("Exception [forceReload HTTPServersLoader] --> " + e);
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.fatal("Exception in fetchServerInfoFromDB: ", e);
                    noError = false;
                }
            }
            if (!authControllerUrlsLocal.isEmpty()) {
                authControllerUrls = authControllerUrlsLocal;
            }
            if (noError) {
                updateTime = System.currentTimeMillis();
            }
        } catch (Exception ex) {
            logger.fatal("DBException in fetchServerInfoFromDB: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private boolean getLoadVal() {
        boolean load = false;

        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT `load` FROM forceLoad WHERE `name`='hb';";
            stmt = db.connection.createStatement();

            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                load = rs.getBoolean("load");
            }

            try {
                stmt.close();
            } catch (Exception e) {
            }

            if (load) {
                sql = "update forceLoad set `load`=0 where name='hb';";
                stmt = db.connection.createStatement();
                stmt.executeUpdate(sql);
            }
        } catch (Exception e) {
            System.err.println("" + e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
            }

            if (db != null && db.connection != null) {
                try {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                } catch (Exception e) {
                }
            }
        }

        return load;
    }

    public ArrayList<String> getAuthControllerUrls() {
        return authControllerUrls;
    }

}
