/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.http;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.log4j.Logger;
import server.util.Constants;
import server.util.StorageQueue;

/**
 *
 * @author rejuan
 */
public class HTTPMonitor extends Thread {

    private static HTTPMonitor instance;
    private boolean running;
    public ConcurrentHashMap<String, HTTPServerDTO> liveServersMapHTTP;
    public ConcurrentHashMap<String, HTTPServerDTO> deadServersMapHTTP;
    private static final Logger logger = Logger.getLogger(HTTPMonitor.class.getName());

    public static HTTPMonitor getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new HTTPMonitor();
        }
    }

    public HTTPMonitor() {
        this.running = true;
    }

    @Override
    public void run() {
        while (running) {
            boolean serverDown = false;
            boolean serverLive = false;
            try {
                HTTPServersLoader.getInstance().checkForReload();
                liveServersMapHTTP = HTTPServersLoader.getInstance().getLiveServersMapHTTP();
                deadServersMapHTTP = HTTPServersLoader.getInstance().getDeadServersMapHTTP();

                serverDown = checkLiveServerStatus(liveServersMapHTTP);
                serverLive = checkDeadServerStatus(deadServersMapHTTP);
                logger.debug("liveServersMapHTTP --> " + liveServersMapHTTP.size() + " --> deadServersMapHTTP --> " + deadServersMapHTTP.size() + " --> serverDown --> " + serverDown + " --> serverLive --> " + serverLive);
            } catch (Exception e) {
                logger.error("Exception [HTTPMonitor] --> " + e);
            }
            if (running) {
                sleepAndLoad(serverDown || serverLive);
            }
        }
        logger.debug("--------------- HTTPMonitor Thread finished ---------------");
    }

    private boolean checkLiveServerStatus(ConcurrentHashMap<String, HTTPServerDTO> liveServersMapHTTP) {
        boolean serverDown = false;
        logger.debug("liveServersMapHTTP size --> " + liveServersMapHTTP.size());
        for (Map.Entry<String, HTTPServerDTO> entry : liveServersMapHTTP.entrySet()) {
            if (running) {
                HTTPServerDTO httpServerDTO = entry.getValue();
                try {
                    if (httpServerDTO.isSecured()) {
                        serverDown = checkSecuredHTTPServerStatus(httpServerDTO);
                    } else {
                        serverDown = checkHTTPServerStatus(httpServerDTO);
                    }
                    if (serverDown) {
                        httpServerDTO.setAttemptNumber(httpServerDTO.getAttemptNumber() + 1);
                        if (httpServerDTO.getAttemptNumber() >= Constants.MAX_ATTEMPT) {
                            serverDown(httpServerDTO);
                            httpServerDTO.setAttemptNumber(0);
                            logger.error("[Exception && exceeds MAX_ATTEMPT] --> " + httpServerDTO.getServerIP() + ":" + httpServerDTO.getServerPort() + " ==> DEAD");
                        }
                    }
                } catch (Exception e) {
                    logger.error("Exception [HTTPMonitor] --> " + e);
                }
            }
        }
        return serverDown;
    }

    private boolean checkDeadServerStatus(ConcurrentHashMap<String, HTTPServerDTO> deadServersMapHTTP) {
        boolean serverLive = false;
        for (Map.Entry<String, HTTPServerDTO> entry : deadServersMapHTTP.entrySet()) {
            if (running) {
                HTTPServerDTO httpServerDTO = entry.getValue();
                try {
                    int responseCode;
                    if (httpServerDTO.isSecured()) {
                        HTTPSClient httpClient = new HTTPSClient(httpServerDTO.getServerURL(), httpServerDTO.getHost());
                        httpClient.visitUrl("GET");
                        responseCode = httpClient.getResponseCode();
                        httpClient.finish();
                    } else {
                        HTTPClient httpClient = new HTTPClient(httpServerDTO.getServerURL());
                        responseCode = httpClient.getResponseCode();
                        httpClient.finish();
                    }

                    if (responseCode == 200) {
                        httpServerDTO.setServerAliveStatus(Constants.SERVER_STATUS_ALIVE);
                        //StorageQueue.getInstance().putIPPort(httpServerDTO.getServerIP() + ":" + httpServerDTO.getServerPort());
                        StorageQueue.getInstance().putIPPort(httpServerDTO);
                        logger.debug("A dead server has become ALIVE --> " + httpServerDTO.getServerURL());
                        serverLive = true;

                        serverUp(httpServerDTO);
                    }
                } catch (Exception e) {
                    // Ignored, as this server was already deaD!
                }
            }
        }
        return serverLive;
    }

    private void serverDown(HTTPServerDTO httpServerDTO) {
        liveServersMapHTTP.remove(httpServerDTO.getServerIP() + ":" + httpServerDTO.getServerPort());
        deadServersMapHTTP.put(httpServerDTO.getServerIP() + ":" + httpServerDTO.getServerPort(), httpServerDTO);

        httpServerDTO.setServerAliveStatus(Constants.SERVER_STATUS_DEAD);
        //StorageQueue.getInstance().putIPPort(httpServerDTO.getServerIP() + ":" + httpServerDTO.getServerPort());
        StorageQueue.getInstance().putIPPort(httpServerDTO);
        logger.debug("Server IP:" + httpServerDTO.getServerIP() + ", Port:" + httpServerDTO.getServerPort() + ", Type:" + httpServerDTO.getServerType() + " is going down@ " + new java.util.Date());
    }

    private void serverUp(HTTPServerDTO httpServerDTO) {
        deadServersMapHTTP.remove(httpServerDTO.getServerIP() + ":" + httpServerDTO.getServerPort());
        liveServersMapHTTP.put(httpServerDTO.getServerIP() + ":" + httpServerDTO.getServerPort(), httpServerDTO);

        httpServerDTO.setServerAliveStatus(Constants.SERVER_STATUS_ALIVE);
        //StorageQueue.getInstance().putIPPort(httpServerDTO.getServerIP() + ":" + httpServerDTO.getServerPort());
        StorageQueue.getInstance().putIPPort(httpServerDTO);
        logger.debug("Server IP:" + httpServerDTO.getServerIP() + ", Port:" + httpServerDTO.getServerPort() + ", Type:" + httpServerDTO.getServerType() + " is going live@ " + new java.util.Date());
    }

    public void stopService() {
        this.running = false;
        this.interrupt();
    }

    private void sleepAndLoad(boolean load) {
        try {
            Thread.sleep(30000);
//            if (load) {
//                HTTPServersLoader.getInstance().forceReload();
//            }
        } catch (Exception e) {
            logger.error("Exception [sleepAndLoad] --> " + e);
        }
    }

    private boolean checkHTTPServerStatus(HTTPServerDTO httpServerDTO) throws IOException {
        try {
            HTTPClient httpClient = new HTTPClient(httpServerDTO.getServerURL());
            int responseCode = httpClient.getResponseCode();
            httpClient.finish();

            if (responseCode == 200) {
                httpServerDTO.setAttemptNumber(0);
                return false;
            } else {
                logger.error("serverDown [responseCode]--> " + responseCode + " --> " + httpServerDTO.getServerURL());
                return true;
            }
        } catch (Exception e) {
            logger.error("Exception [checkHTTPServerStatus] --> ", e);
            return true;
        }
    }

    private boolean checkSecuredHTTPServerStatus(HTTPServerDTO httpServerDTO) {
        try {
            HTTPSClient hTTPSClient = new HTTPSClient(httpServerDTO.getServerURL(), httpServerDTO.getHost());
            hTTPSClient.visitUrl("GET");

            if (hTTPSClient.getResponseCode() == 200) {
                httpServerDTO.setAttemptNumber(0);
                return false;
            } else {
                logger.error("serverDown [responseCode]--> " + hTTPSClient.getResponseCode() + " --> " + httpServerDTO.getServerURL());
                return true;
            }
        } catch (Exception e) {
            logger.error("Exception [checkSecuredHTTPServerStatus] --> ", e);
            return true;
        }
    }

    public static void main(String[] args) {
        HTTPMonitor monitor = new HTTPMonitor();
        HTTPServerDTO httpServerDTO = new HTTPServerDTO();
        httpServerDTO.setServerURL("https://104.193.36.36:443");
        httpServerDTO.setHost("www.ringid.com");
        System.out.println(monitor.checkSecuredHTTPServerStatus(httpServerDTO));
    }
}
