package server.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import org.apache.log4j.Logger;

public class TCPClient {

    private static final Logger logger = Logger.getLogger(HTTPServersLoader.class.getName());

    public static String sendRequest(InetAddress serverIP, int port, String request) {
        PrintWriter out;
        BufferedReader in;
        String response = null;
        try {
            Socket clientSocket = new Socket(serverIP, port);
            clientSocket.setSoTimeout(5000);

            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            out.println(request);

            char[] buff = new char[256];
            in.read(buff);

            response = new String(buff).trim();

            out.close();
            in.close();
        } catch (IOException | NullPointerException | IllegalArgumentException | SecurityException e) {
            logger.error("Couldn't get I/O for the connection to " + serverIP + "->", e);
        }

        return response;
    }

    public static void main(String[] args) {
        try {
            TCPClient.sendRequest(InetAddress.getByName("192.168.1.185"), 9500, "reloadauth");
            TCPClient.sendRequest(InetAddress.getByName("192.168.1.185"), 9500, "reload-chat-servers");
            TCPClient.sendRequest(InetAddress.getByName("192.168.1.185"), 9500, "reload-voice-servers");

            TCPClient.sendRequest(InetAddress.getByName("192.168.1.185"), 9500, "reload-offline-servers");
            TCPClient.sendRequest(InetAddress.getByName("192.168.1.185"), 9500, "reload-streaming-servers");
            TCPClient.sendRequest(InetAddress.getByName("192.168.1.185"), 9500, "reload-channel-servers");

            TCPClient.sendRequest(InetAddress.getByName("192.168.1.185"), 9500, "reload-coin-servers");
            TCPClient.sendRequest(InetAddress.getByName("192.168.1.185"), 9500, "reload-streaming-media-servers");
            TCPClient.sendRequest(InetAddress.getByName("192.168.1.185"), 9500, "reload-ringbit-servers");

            TCPClient.sendRequest(InetAddress.getByName("192.168.1.185"), 9500, "reload-sms-servers");
            TCPClient.sendRequest(InetAddress.getByName("192.168.1.185"), 9500, "reload-market-servers");
            TCPClient.sendRequest(InetAddress.getByName("192.168.1.185"), 9500, "reload-tv-channel-servers");
        } catch (UnknownHostException ex) {

        }
    }
}
