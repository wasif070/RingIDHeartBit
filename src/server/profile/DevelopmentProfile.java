package server.profile;

public class DevelopmentProfile {

    public static final String AUTH_CONTROLLER_SUFFIX = "/rac/comports?lt=1&amp;ringID=2110063638";
    public static final String IMAGE_SUFFIX = "/ringmarket/StickerHandler?all=1";
    public static final String CLOUD_SUFFIX = "/uploaded/2110063638/pthumb1442297440440.jpg";
    public static final String COIN_SERVER_SUFFIX = "/api/userwallet/getuserwalletstatus?userId=55650";
}
