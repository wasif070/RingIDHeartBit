package server.profile;

/**
 *
 * @author reefat
 */
public class ProductionProfile {

    // NOT-CONFIGURED-YET
    public static final String AUTH_CONTROLLER_SUFFIX = "/rac/comports?lt=1&ringID=2110063638";
    public static final String IMAGE_SUFFIX = "/ringmarket/StickerHandler?all=1";
    public static final String CLOUD_SUFFIX = "/index.html";
    public static final String COIN_SERVER_SUFFIX = "/api/userwallet/getuserwalletstatus?userId=17024";
    public static final String RINGBIT_SERVER_SUFFIX = "/investmentcoin/api/wallet/17024";
    public static final String SMSBD_SERVER_SUFFIX = "/";
}
