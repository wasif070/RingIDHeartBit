package server;

import java.net.DatagramPacket;
import org.apache.log4j.Logger;
import server.db.DBUpdater;
import server.db.ServersLoader;
import server.db.ServerDTO;
import server.parser.Attributes;
import server.parser.PacketParser;
import server.util.Actions;
import server.util.Constants;
import server.util.StatisticsSender;
import server.util.StorageQueue;
import server.util.ThreadExecutor;

public class ReceivedPacketProcessor extends Thread {

    private static ReceivedPacketProcessor instance;
    private boolean running = false;
    private static final long SLEEP_INTERVAL = 100L;
    private static final Logger logger = Logger.getLogger(ReceivedPacketProcessor.class.getName());

    public static ReceivedPacketProcessor getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new ReceivedPacketProcessor();
        }
    }

    public ReceivedPacketProcessor() {
        running = true;
    }

    @Override
    public void run() {
        logger.debug("ReceivedPacketProcessor is started.....");
        while (running) {
            while (running && !StorageQueue.getInstance().getPacketQueue().isEmpty()) {
                try {
                    DatagramPacket receivedPacket = StorageQueue.getInstance().getPacketQueue().poll();
                    int dataLen = receivedPacket.getLength();
                    byte[] dataBytes = new byte[dataLen];
                    System.arraycopy(receivedPacket.getData(), 0, dataBytes, 0, dataLen);

                    int serverType = (dataBytes[0] & 0xFF);
//                    int serverPort = getInt(dataBytes, 1) - serverType;
//                    long serverIP = getLong(dataBytes, 5) - serverType - serverPort;

                    if (serverType > 0) {
                        int serverPort = receivedPacket.getPort();
                        String IP = receivedPacket.getAddress().getHostAddress();
                        String IPPort = IP + ":" + serverPort;
                        if (Constants.HEAVY_LOG_ENABLE) {
                            logger.debug("Ping from IPPort:" + IPPort + ", serverType:" + serverType);
                        }

                        ServerDTO serverInfoDTO = ServersLoader.getInstance().getServerDTO(IPPort);
                        boolean serverFound = false;
                        if (serverInfoDTO != null && serverInfoDTO.getServerType() == serverType) {
                            serverInfoDTO.setServerLastAliveTime(System.currentTimeMillis());
                            if (serverInfoDTO.getServerAliveStatus() == Constants.SERVER_STATUS_DEAD) {
                                serverInfoDTO.setServerAliveStatus(Constants.SERVER_STATUS_ALIVE);
                                serverInfoDTO.setMessageSentTime(0);
                                serverInfoDTO.setMessageSentCount(0);
                                StorageQueue.getInstance().putIPPort(serverInfoDTO);
                                logger.debug("Server IP:" + serverInfoDTO.getServerIP() + ", Port:" + serverInfoDTO.getServerPort() + ", Type:" + serverInfoDTO.getServerType() + " is going live @" + new java.util.Date());
                            }
                            serverFound = true;
                        } else {
                            int validity = ServersLoader.getInstance().isValidRequest(IP, serverPort, serverType);
                            if (validity >>> 16 > 0) {
                                ServerDTO newServerDTO = new ServerDTO();
                                newServerDTO.setServerIP(IP);
                                newServerDTO.setServerPort(serverPort);
                                newServerDTO.setTcpPort(validity & 0x0000FFFF);
                                newServerDTO.setServerType(serverType);
                                newServerDTO.setServerAliveStatus(Constants.SERVER_STATUS_ALIVE);
                                newServerDTO.setServerLastAliveTime(System.currentTimeMillis());
                                ServersLoader.getInstance().putServerDTO(IPPort, newServerDTO);
                                StorageQueue.getInstance().putIPPort(newServerDTO);
                                logger.debug("Server IP:" + newServerDTO.getServerIP() + ", Port:" + newServerDTO.getServerPort() + ", Type:" + newServerDTO.getServerType() + " is going live @" + new java.util.Date());
                                serverFound = true;
                            }
                        }

                        if (serverFound && dataLen > 13) {
                            switch (serverType) {
                                case Constants.SERVER_TYPE_AUTH: {
                                    DBUpdater updater = new DBUpdater(serverType, IP, serverPort);
                                    Attributes attributes = PacketParser.parsePacket(dataBytes, 13);
                                    updater.setAuthAttributes(attributes);
                                    ThreadExecutor.getInstance().execute(updater);
                                    break;
                                }
                                case Constants.SERVER_TYPE_CHAT: {
                                    DBUpdater updater = new DBUpdater(serverType, IP, serverPort);
                                    Attributes attributes = PacketParser.parsePacket(dataBytes, 13);
                                    updater.setChatAttributes(attributes);
                                    ThreadExecutor.getInstance().execute(updater);
                                    break;
                                }
                                case Constants.SERVER_TYPE_VOICE: {
                                    DBUpdater updater = new DBUpdater(serverType, IP, serverPort);
                                    Attributes attributes = PacketParser.parsePacket(dataBytes, 13);
                                    updater.setVoiceAttributes(attributes);
                                    ThreadExecutor.getInstance().execute(updater);
                                    break;
                                }
                                case Constants.SERVER_TYPE_LIVE_STREAM: {
                                    DBUpdater updater = new DBUpdater(serverType, IP, serverPort);
                                    Attributes attributes = PacketParser.parsePacket(dataBytes, 13);
                                    updater.setStreamAttributes(attributes);
                                    ThreadExecutor.getInstance().execute(updater);
                                    break;
                                }
                                case Constants.SERVER_TYPE_CHANNEL: {
                                    DBUpdater updater = new DBUpdater(serverType, IP, serverPort);
                                    Attributes attributes = PacketParser.parsePacket(dataBytes, 13);
                                    updater.setChannelAttributes(attributes);
                                    ThreadExecutor.getInstance().execute(updater);
                                    break;
                                }
                                default:
                                    break;
                            }
                        }
                    } else {
                        Attributes attributes = PacketParser.parsePacket(dataBytes, 1);
                        switch (attributes.getAction()) {
                            case Actions.ACTION_SEND_STAT:
                                StatisticsSender.getInstance().sendStatistics();
                                break;
                            default:
                                break;
                        }
                    }
                } catch (Exception ex) {
                    logger.fatal("Error in ReceivedPacketProcessor: ", ex);
                }
            }
            try {
                if (running) {
                    Thread.sleep(SLEEP_INTERVAL);
                }
            } catch (InterruptedException ex) {
            }
        }
    }

    public void stopService() {
        running = false;
    }

    private String getServerIP(long serverIP) {
        return ((serverIP >> 24) & 0xFF) + "." + ((serverIP >> 16) & 0xFF) + "." + ((serverIP >> 8) & 0xFF) + "." + (serverIP & 0xFF);
    }

    private int getInt(byte[] data_bytes, int index) {
        return (data_bytes[index++] & 0xFF) << 24 | (data_bytes[index++] & 0xFF) << 16 | (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);

    }

    private int getShort(byte[] data_bytes, int index) {
        return (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);

    }

    private long getLong(byte[] data_bytes, int index) {
        long result = 0;
        for (int i = 7; i > 0; i--) {
            result |= (data_bytes[index++] & 0xFFL) << (8 * i);
        }
        result |= (data_bytes[index++] & 0xFFL);

        return result;
    }

    public static int addInt(int attribute, int value, int length, byte[] send_bytes, int index) {
        send_bytes[index++] = (byte) (attribute);
        send_bytes[index++] = (byte) (length);

        switch (length) {
            case 2:
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            case 3:
                send_bytes[index++] = (byte) (value >> 16);
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            case 4:
                send_bytes[index++] = (byte) (value >> 24);
                send_bytes[index++] = (byte) (value >> 16);
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
        }
        return index;
    }
//
//    public static void main(String[] args) {
//        byte[] data = new byte[28];
//        int index = 13;
//        index = ReceivedPacketProcessor.addInt(1, 200000, 3, data, index);
//        index = ReceivedPacketProcessor.addInt(2, 200000, 3, data, index);
//        index = ReceivedPacketProcessor.addInt(4, 1000, 3, data, index);
//
//        PacketParser.parsePacket(data, 13);
//        System.out.println(index);
//    }
}
