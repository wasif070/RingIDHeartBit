package server;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.log4j.Logger;
import server.db.ServersLoader;
import server.db.ServerDTO;
import server.util.Constants;
import server.util.StorageQueue;

public class ServerStatusProcessor extends Thread {

    private static ServerStatusProcessor instance;
    private boolean running = false;
    private static final long SLEEP_INTERVAL = 10000L;
    private static final Logger logger = Logger.getLogger(ServerStatusProcessor.class.getName());

    public static ServerStatusProcessor getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new ServerStatusProcessor();
        }
    }

    public ServerStatusProcessor() {
        running = true;
    }

    @Override
    public void run() {
        logger.debug("ServerStatusProcessor is started.....");
        while (running) {
            try {
                ConcurrentHashMap<String, ServerDTO> serverMap = ServersLoader.getInstance().getServersMap();
                for (Map.Entry<String, ServerDTO> entry : serverMap.entrySet()) {
                    ServerDTO serverDTO = entry.getValue();

                    if ((System.currentTimeMillis() - serverDTO.getServerLastAliveTime()) > Constants.EXPIRE_TIME) {
                        serverDTO.setServerAliveStatus(Constants.SERVER_STATUS_DEAD);
                        if (System.currentTimeMillis() - serverDTO.getMessageSentTime() > Constants.MESSAGE_SENT_INTERVAL) {
                            StorageQueue.getInstance().putIPPort(serverDTO);
                            serverDTO.setMessageSentTime(System.currentTimeMillis());
                            serverDTO.setMessageSentCount(serverDTO.getMessageSentCount() + 1);
                            if (serverDTO.getMessageSentCount() > 3) {
                                logger.debug("Server IP:" + serverDTO.getServerIP() + ", Port:" + serverDTO.getServerPort() + ", Type:" + serverDTO.getServerType() + " will be removed from server map, map size " + ServersLoader.getInstance().getServersMap().size());
                                ServersLoader.getInstance().getServersMap().remove(entry.getKey());
                                logger.debug("Server IP:" + serverDTO.getServerIP() + ", Port:" + serverDTO.getServerPort() + ", Type:" + serverDTO.getServerType() + " has been removed from server map, map size " + ServersLoader.getInstance().getServersMap().size());
                            }
                        }
                        logger.debug("Server IP:" + serverDTO.getServerIP() + ", Port:" + serverDTO.getServerPort() + ", Type:" + serverDTO.getServerType() + " is going down@ " + new java.util.Date());
                    }
                }
            } catch (Exception ex) {
                logger.fatal("Error in ServerStatusProcessor: ", ex);
            }
            try {
                if (running) {
                    Thread.sleep(SLEEP_INTERVAL);
                }
            } catch (Exception ex) {
            }
        }
    }

    public void stopService() {
        running = false;
        this.interrupt();
    }
}
