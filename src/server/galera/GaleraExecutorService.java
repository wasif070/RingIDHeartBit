/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.galera;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import server.util.SendSmsService;

/**
 *
 * @author Faiz
 */
public class GaleraExecutorService {

    private static boolean isrunning = true;
    private final String appString;
    private final String serverIP;
    private final String userName;
    private final String password;
    private final String[] phoneNumbers;
    private final String command = GaleraConstants.COMMAND;
    private final String commandFailedOutput = "Unable to connect";

    public GaleraExecutorService(String appString, String serverIP, String userName, String password, String[] phoneNumbers) {
        this.appString = appString;
        this.password = password;
        this.userName = userName;
        this.serverIP = serverIP;
        this.phoneNumbers = phoneNumbers;
    }
    private final Logger LOGGER = Logger.getLogger(GaleraExecutorService.class.getName());

    public void startSchedulerService() {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        Runnable processor = () -> {
            try {
                LOGGER.info(appString + "(" + new Date() + "):Trying to connect with " + serverIP + " ...");
                SSHManager instance = new SSHManager(this.userName, this.password, this.serverIP, "");
                String message = instance.connect();
                LOGGER.info(appString + ": Connection result for " + serverIP + "=> " + message);
                if (message == null) {
                    String data = instance.sendCommand(command);
                    if (data != null && data.trim().length() > 0 && !data.contains(commandFailedOutput)) {
                        prcessData(data);
                    } else {
                        LOGGER.error(appString + ": Command '" + command + "' execution in " + serverIP + " failed, result =>" + data);
                        maxadminCommandExecutionFailed(serverIP);
                    }
                    instance.close();
                } else {
                    LOGGER.error(appString + " :" + message);
                    monitoringAdminDownMessage(serverIP);
                }
                if (!isrunning) {
                    service.shutdown();
                    setRunning(false);
                }
            } catch (Exception e) {
                LOGGER.error(e, e);
            } finally {
            }
        };
        service.scheduleAtFixedRate(processor, 0, 10, TimeUnit.MINUTES);
    }

    /**
     * <b>Out of the command "sudo maxadmin list servers"</b>
     * -------------------+-----------------+-------+-------------+--------------------
     * Server | Address | Port | Connections | Status
     * -------------------+-----------------+-------+-------------+--------------------
     * galeradb1 | 192.168.246.59 | 3306 | 0 | Slave, Synced, Running
     *
     * galeradb2 | 192.168.246.74 | 3306 | 0 | Slave, Synced, Running
     *
     * galeradb3 | 192.168.246.75 | 3306 | 0 | Master, Synced, Running
     * -------------------+-----------------+-------+-------------+--------------------
     * <p>
     * <b>Out put of Command "maxadmin list servers |grep 3306 | cut -d '|'
     * -f2,5"
     *
     * 192.168.246.59 | Slave, Synced, Running
     *
     * 192.168.246.74 | Slave, Synced, Running
     *
     * 192.168.246.75 | Master, Synced, Running
     *
     *
     * @param outString
     */
    private void prcessData(String outString) {
        LOGGER.info(appString + " :" + outString);
        String[] splitByNewLine = outString.split("\\r?\\n");
        for (String lines : splitByNewLine) {
            checkServerOk(lines);
        }
    }

    public void stopReloader() {
        setRunning(false);
    }

    /**
     * Server is ok of inputString contains "Running"
     *
     * @param inputString "galeradb1 | 192.168.246.59 | Slave, Synced, Running"
     * or "192.168.246.59 | Down"
     */
    private void checkServerOk(String inputString) {
        if (!inputString.contains(GaleraConstants.VALID_STRING)) {
            LOGGER.error(appString + " :" + inputString);
            String[] secondSplit = inputString.split("\\|");
            if (secondSplit.length > 0) {
                String smsBody = "'" + secondSplit[0].trim() + "'" + "%20'" + secondSplit[1].trim() + "'%20is%20disconnected" + "%20from%20" + appString + "%20cluster!";
                new SendSmsService(phoneNumbers, smsBody).start();
            }
        }
    }

    private void monitoringAdminDownMessage(String ip) {
        String smsBody = appString + ":%20Maxadmin%20'" + ip + "'" + "%20is%20dead!";
        new SendSmsService(phoneNumbers, smsBody).start();
    }

    private void maxadminCommandExecutionFailed(String ip) {
        String smsBody = appString + "%20'" + ip + "'" + "%20'maxadmin'%20command%20execution%20failed!";
        new SendSmsService(phoneNumbers, smsBody).start();
    }

    public static void setRunning(boolean value) {
        isrunning = value;
    }

//    public static void main(String[] args) {
//        GaleraExecutorService dd= new GaleraExecutorService();
//        dd.checkServerOk("galeradb1 | 192.168.246.59 | Slave, Synced");
//    }
}
